<?php namespace App\Models;

class Shop_model {

    
	public function __construct() {   
		
		
    }
	
	function shop_read($shopid) 
	{
		$shop_xml_url = config('Simunix')->cfg_data['xml_server'].'shopxml.aspx';
		$url = $shop_xml_url
		. '?cd=shopread&enc_shop_id=' . $shopid;
		
		
		$result = simplexml_load_file($url);	
		
		if($result->valid && $result->valid->shoptransaction) {
			return $result->valid->shoptransaction;
		}
		
		return FALSE;
	}
}