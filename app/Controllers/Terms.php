<?php namespace App\Controllers;

class Terms extends BaseController
{
    public function getIndex()
    {
        $metatitle = "Simunix - Terms &amp; Conditions";
        $metadesc  = "Find out information regarding our terms &amp; conditions";

        return $this->getTemplate('terms', $metatitle, $metadesc);
    }
}