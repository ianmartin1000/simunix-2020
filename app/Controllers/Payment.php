<?php namespace App\Controllers;

class Payment extends BaseController
{
	//$payment_info;
	public function __construct() {
		
		$this->shop_model = new \App\Models\Shop_Model();
	}
    public function getIndex()
    {
        $metatitle = "Simunix - Payment";
        $metadesc  = "Established in 1998, Simunix was the first non-telecom company to bring free directory enquiries to the Internet as www.ukphonebook.com.";
		
		$id = $_GET['id'];
		$payment_info = false;
		if ($id){		
			$payment_info = $this->shop_model->shop_read($id);
		}
		
		$page_data = [];
		$page_data['payment_info'] = $payment_info;
		
		return $this->getTemplate('payment', $metatitle, $metadesc, null, $page_data);
    }
}