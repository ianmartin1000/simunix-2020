<?php namespace App\Controllers;

class About extends BaseController
{
    public function getIndex()
    {
        $metatitle = "Simunix - About Us";
        $metadesc  = "Established in 1998, Simunix was the first non-telecom company to bring free directory enquiries to the Internet as www.ukphonebook.com.";

        return $this->getTemplate('about', $metatitle, $metadesc);
    }
}