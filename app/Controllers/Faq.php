<?php namespace App\Controllers;

class Faq extends BaseController
{
    public function getIndex()
    {
        $metatitle = "Simunix - FAQs";
        $metadesc  = "Find the answers to the most frequently asked questions of us";

        return $this->getTemplate('faq', $metatitle, $metadesc);
    }
}