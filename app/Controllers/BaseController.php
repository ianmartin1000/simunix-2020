<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */
abstract class BaseController extends Controller
{
    /**
     * Instance of the main Request object.
     *
     * @var CLIRequest|IncomingRequest
     */
    protected $request;

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var list<string>
     */
    protected $helpers = ['general_helper'];

    /**
     * Be sure to declare properties for any property fetch you initialized.
     * The creation of dynamic property is deprecated in PHP 8.2.
     */
    // protected $session;

    /**
     * @return void
     */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        // Preload any models, libraries, etc, here.

        // E.g.: $this->session = \Config\Services::session();

        $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'simunix.com';

        if ($host == 'www.simunix.com') {
            $current_controller = $request->uri->getSegment(1);
            $redir = 'https://simunix.com';
            if ($current_controller) {
                $redir .= '/' . $current_controller;
            }

            $this->controllerRedirect($redir);
        }
    }

    function log_ukpb_message($txt)
    {
        $filepath = WRITEPATH . 'logs/ukpb-log-'.date('Y-m-d').'.log';
        $message  = '';


        if ( ! $fp = @fopen($filepath, 'ab'))
        {
            return FALSE;
        }

        $message .= date('Y-m-d H:i:s'). ' --> '.$txt."\n";



        flock($fp, LOCK_EX);
        fwrite($fp, $message);
        flock($fp, LOCK_UN);
        fclose($fp);

        @chmod($filepath, FILE_WRITE_MODE);
        return TRUE;
    }

    protected function getTemplate($page, $metatitle, $metadesc, array $pageNav = null, $page_data = null) {

        $this->simunixConfig = new \Config\Simunix();


        $data = [];

        $data['pageNav']['elements'] = $pageNav;
        $data['version']                = $this->simunixConfig->cfg_data['version'];
        $data['page']                = $page;
        $data['css_files']           = [$page];
        $data['js_files']            = [$page];
        $data['metatitle']           = $metatitle;
        $data['metadesc']           = $metadesc;
        if ($page_data != null)
        {
            foreach($page_data as $k => $v)
            {
                $data[$k] = $v;
            }
        }
        $data['content']           = view($page, $data);
        return view('template', $data);
    }

        function send_authsmtp_email($subject,$html_msg)
        {

            //log_message('critical','send_authsmtp_email:'.$subject."-".$html_msg);

            $email = \Config\Services::email();

            $email->setFrom('noreply@notifyukpb.com');
            $email->setTo('info@simunix.com');

            $email->setSubject($subject);
            $email->setMessage($html_msg);

            $email->send();

        }

        protected function reCaptcha_Decr($id)
    {
        $this->myrequest = \Config\Services::request();
        $this->simunixConfig = new \Config\Simunix();
        $url = $this->simunixConfig->cfg_data['xml_server'].'shopxml.aspx?cd=shopdecrid'
            . '&id=' . urlencode($id);

        //log_message('critical','reCaptcha_Decr:'.$this->myrequest->getIPAddress().$url);

        $result = simplexml_load_file($url);
        if($result->valid)
        {

            $decr = $result->valid->raw_id;
            $arr = explode("|",$decr);
            $prev_ip = $arr[0];
            $prev_time = $arr[1];

            if ($this->myrequest->getIPAddress() == $prev_ip){ //if they swap ip they must do captcha again
                if (((int)$prev_time + 120) > time()) //we store ip so they only do captcha once per day, this time is time it takes to complete  captcha and click submit and load results
                {

                    return TRUE;

                }
                else
                {
                    return FALSE;
                }
            }
        }
        else
        {
            return FALSE;
        }
    }
}
