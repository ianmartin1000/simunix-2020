<?php namespace App\Controllers;

class SiteMap extends BaseController
{
    public function getIndex()
    {
        $metatitle = "Simunix - Site Map";
        $metadesc  = "Site map of simunix.com";

        return $this->getTemplate('sitemap', $metatitle, $metadesc);
    }
}