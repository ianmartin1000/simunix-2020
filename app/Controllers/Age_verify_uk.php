<?php namespace App\Controllers;

class Age_verify_uk extends BaseController
{
	
	public $metatitle = "Simunix - Age Verify UK - Ensure Age Compliance with Our Secure Verification Solutions";
    public $metadesc  = "Our UK age verification service matches your customer’s details against 120 million UK person addresses to verify if they are aged 18 or over.";
	
	
	public function postIndex()
    {	
		return $this->getIndex();
		
	}
	


    public function getIndex()
    {
		
		//$this->send_authsmtp_email("Simunix.com ORBIS contact form - TESTER","<p>here <strong>ok</strong></p>");
		
        $pageNav = [
            "keyFeatures"  => "Key Features",
            "solutions"    => "Solutions",
            "ourCustomers" => "Our Customers",
            "contact"      => "Contact Us",
        ];

        

       
        if(isset($_POST) && isset($_POST['captcha_key'])) {

			$captcha_error = FALSE;

			$resp = $_POST['captcha_key'];
			$captcha_error = !$this->reCaptcha_Decr($resp);

			if ($captcha_error){
				echo "<h1>Captcha Error</h1>";
				echo "<p>Please <a href=\"".base_url("age-verify-uk")."\">try again</a></p>";
				die;
			}else{
				$name         = $_POST['first'] . " " . $_POST["last"];
				$phone        = $_POST['phone'];
				$company      = $_POST['company'];
				$email        = $_POST['email'];
				

				$html_msg = "
				<h1>New AVUK enquiry from simunix.com</h1>				
				<p><strong>From: </strong>{$name} ({$email})</p>
				<p><strong>Company: </strong>{$company}</p>
				<p><strong>Phone: </strong>{$phone}</p>
				";

				$this->send_authsmtp_email("Simunix.com Age Verify UK contact form - general enquiry",$html_msg);
			}
        }
		return $this->getTemplate('avuk', $this->metatitle, $this->metadesc, $pageNav);
    }


}
