<?php namespace App\Controllers;

class Orbis extends BaseController
{
	
	 public $metatitle = "Simunix - ORBIS - Comprehensive Search and Data Solutions for UK Businesses";
     public $metadesc  = "ORBIS brings together people and business information from a variety of government, commercial and opted-in marketing sources and makes it available for searching, analysis, tele-appending or download to your desktop.";
	

	public function postIndex()
	{
		return $this->getIndex();

	}		
    public function getIndex()
    {
		
		//$this->send_authsmtp_email("Simunix.com ORBIS contact form - TESTER","<p>here <strong>ok</strong></p>");
		
        $pageNav = [
            "keyFeatures"  => "Key Features",
            "sectors"      => "Sectors",
            "ourCustomers" => "Our Customers",
            "dataSources"  => "Data Sources",
            "pricing"      => "Pricing",
        ];

        if(isset($_POST) && isset($_POST['captcha_key'])) {

			$captcha_error = FALSE;

			$resp = $_POST['captcha_key'];
			$captcha_error = !$this->reCaptcha_Decr($resp);

			if ($captcha_error){
				echo "<h1>Captcha Error</h1>";
				echo "<p>Please <a href=\"".base_url("Orbis")."\">try again</a></p>";
				die;
			}else{
				$name         = $_POST['first'] . " " . $_POST["last"];
				$phone        = $_POST['phone'];
				$company      = $_POST['company'];
				$email        = $_POST['email'];
				$enquiry      = $_POST['enquiry'];

				$html_msg = "
				<h1>New ORBIS enquiry from simunix.com</h1>
				<p><strong>Enquiry Type: </strong>{$enquiry}</p>
				<p><strong>From: </strong>{$name} ({$email})</p>
				<p><strong>Company: </strong>{$company}</p>
				<p><strong>Phone: </strong>{$phone}</p>
				";

				$this->send_authsmtp_email("Simunix.com ORBIS contact form - general enquiry",$html_msg);
			}
        }

       

        return $this->getTemplate('orbis', $this->metatitle, $this->metadesc, $pageNav);
    }
	
	public function postLegal()
    {
		return $this->getLegal();
	}

    public function getLegal()
    {


		$this->metatitle = "Simunix - Sectors that use ORBIS: Legal";
		$this->metadesc  = "ORBIS is used by Law Firms &amp; Solicitors for finding people in the UK and verifying their identities.";
		

        $pageNav = [
            "demonstration" => "Demonstration",
            "ourCustomers"  => "Our Customers",
            "testimonials"  => "Testimonials",
            "useCases"      => "Use Cases",
            "pricing"       => "Pricing",
        ];

        if(isset($_POST) && isset($_POST['captcha_key'])) {

            $captcha_error = FALSE;

            $resp = $_POST['captcha_key'];
            $captcha_error = !$this->reCaptcha_Decr($resp);

            if ($captcha_error){
                echo "<h1>Captcha Error</h1>";
                echo "<p>Please <a href=\"".base_url("Orbis")."\">try again</a></p>";
                die;
            }else{
                $name         = $_POST['first'] . " " . $_POST["last"];
                $phone        = $_POST['phone'];
                $company      = $_POST['company'];
                $email        = $_POST['email'];
                $enquiry      = $_POST['enquiry'];

                $html_msg = "
				<h1>New ORBIS enquiry from simunix.com</h1>
				<p><strong>Enquiry Type: </strong>{$enquiry}</p>
				<p><strong>From: </strong>{$name} ({$email})</p>
				<p><strong>Company: </strong>{$company}</p>
				<p><strong>Phone: </strong>{$phone}</p>
				";

                $this->send_authsmtp_email("Simunix.com ORBIS contact form - general enquiry",$html_msg);
            }
        }

        

        return $this->getTemplate('legal', $this->metatitle, $this->metadesc, $pageNav);
    }
	
	public function postPolice()
	{
		return $this->getPolice();
	}

    public function getPolice()
    {
		$this->metatitle = "Simunix - Sectors that use ORBIS: Police &amp; Security";
		$this->metadesc  = "Police forces are using ORBIS to trace individuals &amp; verify identities";

        $pageNav = [
            "power" => "Power of ORBIS",
            "ourCustomers"  => "Our Customers",
            "testimonials"  => "MET Police Testimonial",
            "useCases"      => "Use Cases",
            "pricing"       => "Make an enquiry",
        ];

        if(isset($_POST) && isset($_POST['captcha_key'])) {

            $captcha_error = FALSE;

            $resp = $_POST['captcha_key'];
            $captcha_error = !$this->reCaptcha_Decr($resp);

            if ($captcha_error){
                echo "<h1>Captcha Error</h1>";
                echo "<p>Please <a href=\"".base_url("Orbis")."\">try again</a></p>";
                die;
            }else{
                $name         = $_POST['first'] . " " . $_POST["last"];
                $phone        = $_POST['phone'];
                $company      = $_POST['company'];
                $email        = $_POST['email'];
                $enquiry      = $_POST['enquiry'];

                $html_msg = "
				<h1>New ORBIS enquiry from simunix.com</h1>
				<p><strong>Enquiry Type: </strong>{$enquiry}</p>
				<p><strong>From: </strong>{$name} ({$email})</p>
				<p><strong>Company: </strong>{$company}</p>
				<p><strong>Phone: </strong>{$phone}</p>
				";

                $this->send_authsmtp_email("Simunix.com ORBIS contact form - general enquiry",$html_msg);
            }
        }

       

        return $this->getTemplate('police', $this->metatitle, $this->metadesc, $pageNav);
    }


	public function postLogistics()
	{
		return $this->getLogistics();
	}
	
    public function getLogistics()
    {
		$this->metatitle = "Simunix - Sectors that use ORBIS: Logistics";
		$this->metadesc  = "Logistics Companies use ORBIS to verify customers and aid deliveries.";
		
        $pageNav = [
            "introduction" => "Introduction",
            "ourCustomers" => "Our Customers",
            "testimonials" => "Testimonial",
            "useCases"     => "Use Cases",
            "pricing"      => "Make an enquiry",
        ];

        if(isset($_POST) && isset($_POST['captcha_key'])) {

            $captcha_error = FALSE;

            $resp = $_POST['captcha_key'];
            $captcha_error = !$this->reCaptcha_Decr($resp);

            if ($captcha_error){
                echo "<h1>Captcha Error</h1>";
                echo "<p>Please <a href=\"".base_url("Orbis")."\">try again</a></p>";
                die;
            }else{
                $name         = $_POST['first'] . " " . $_POST["last"];
                $phone        = $_POST['phone'];
                $company      = $_POST['company'];
                $email        = $_POST['email'];
                $enquiry      = $_POST['enquiry'];

                $html_msg = "
				<h1>New ORBIS enquiry from simunix.com</h1>
				<p><strong>Enquiry Type: </strong>{$enquiry}</p>
				<p><strong>From: </strong>{$name} ({$email})</p>
				<p><strong>Company: </strong>{$company}</p>
				<p><strong>Phone: </strong>{$phone}</p>
				";

                $this->send_authsmtp_email("Simunix.com ORBIS contact form - general enquiry",$html_msg);
            }
        }

       

        return $this->getTemplate('logistics', $this->metatitle, $this->metadesc, $pageNav);
    }

	public function postFinance()
    {
		return $this->getFinance();
	}
	
    public function getFinance()
    {
		
		$this->metatitle = "Simunix - Sectors that use ORBIS: Financial";
		$this->metadesc  = "In the financial sector, ORBIS is used cross-departmentally by banks and building societies, from KYC / AML checking, financial crime and fraud investigations to remediation and credit analytics.";
		
        $pageNav = [
            "introduction" => "Introduction",
            "ourCustomers" => "Our Customers",
            "testimonials" => "Testimonial",
            "useCases"     => "Use Cases",
            "pricing"      => "Make an enquiry",
        ];

        if(isset($_POST) && isset($_POST['captcha_key'])) {

            $captcha_error = FALSE;

            $resp = $_POST['captcha_key'];
            $captcha_error = !$this->reCaptcha_Decr($resp);

            if ($captcha_error){
                echo "<h1>Captcha Error</h1>";
                echo "<p>Please <a href=\"".base_url("Orbis")."\">try again</a></p>";
                die;
            }else{
                $name         = $_POST['first'] . " " . $_POST["last"];
                $phone        = $_POST['phone'];
                $company      = $_POST['company'];
                $email        = $_POST['email'];
                $enquiry      = $_POST['enquiry'];

                $html_msg = "
				<h1>New ORBIS enquiry from simunix.com</h1>
				<p><strong>Enquiry Type: </strong>{$enquiry}</p>
				<p><strong>From: </strong>{$name} ({$email})</p>
				<p><strong>Company: </strong>{$company}</p>
				<p><strong>Phone: </strong>{$phone}</p>
				";

                $this->send_authsmtp_email("Simunix.com ORBIS contact form - general enquiry",$html_msg);
            }
        }

        

        return $this->getTemplate('finance', $this->metatitle, $this->metadesc, $pageNav);
    }
	
	public function postEstate_agents()
	{
		return $this->getEstate_agents();
	}

    public function getEstate_agents()
    {
		$this->metatitle = "Simunix - Sectors that use ORBIS: Estate &amp; Letting Agents";
		$this->metadesc  = "Estate &amp; Letting Agents use ORBIS for finding landlord details, doing address history and KYC checks on prospective tenants.";
		

        $pageNav = [
            "howItWorks"    => "How it Works",
            "ourCustomers"  => "Our Customers",
            "testimonials"  => "Testimonials",
            "useCases"      => "Use Cases",
            "pricing"       => "Pricing",
        ];

        if(isset($_POST) && isset($_POST['captcha_key'])) {

            $captcha_error = FALSE;

            $resp = $_POST['captcha_key'];
            $captcha_error = !$this->reCaptcha_Decr($resp);

            if ($captcha_error){
                echo "<h1>Captcha Error</h1>";
                echo "<p>Please <a href=\"".base_url("Orbis")."\">try again</a></p>";
                die;
            }else{
                $name         = $_POST['first'] . " " . $_POST["last"];
                $phone        = $_POST['phone'];
                $company      = $_POST['company'];
                $email        = $_POST['email'];
                $enquiry      = $_POST['enquiry'];

                $html_msg = "
				<h1>New ORBIS enquiry from simunix.com</h1>
				<p><strong>Enquiry Type: </strong>{$enquiry}</p>
				<p><strong>From: </strong>{$name} ({$email})</p>
				<p><strong>Company: </strong>{$company}</p>
				<p><strong>Phone: </strong>{$phone}</p>
				";

                $this->send_authsmtp_email("Simunix.com ORBIS contact form - general enquiry",$html_msg);
            }
        }

       

        return $this->getTemplate('estates', $this->metatitle, $this->metadesc, $pageNav);
    }


}
