<?php namespace App\Controllers;

class Privacy extends BaseController
{
    public function getIndex()
    {
        $metatitle = "Simunix - Privacy";
        $metadesc  = "Find information regarding our privacy policies";

        return $this->getTemplate('privacy', $metatitle, $metadesc);
    }
}