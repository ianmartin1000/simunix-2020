<?php namespace App\Controllers;

class Ukpb extends BaseController
{
    public function getIndex()
    {

        $pageNav = [
            "searchType"  => "Search Type",
            "dataSources" => "Data Sources",
            "keyFeatures" => "Key Features",
            "visitUs"     => "Visit Us",
        ];

        $metatitle = "Simunix - ukphonebook.com - Access the UK’s Most Reliable Business and People Search Tool";
        $metadesc  = "Our public facing ukphonebook.com website allows you to search for people and businesses throughout the UK, including names, addresses, age guides, property prices, company & director reports and much more.";

        return $this->getTemplate('ukpb', $metatitle, $metadesc, $pageNav);
    }

}
