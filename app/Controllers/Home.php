<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        $metatitle = "Simunix - People &amp; Business Data for Search and Analysis";
        $metadesc  = "Bringing together people and business information from a variety of government, commercial and opted-in marketing sources and make it available via a platform to suit you.";
		
		$page_data = [];
		$page_data['canonical_url'] = base_url('');
		
        return $this->getTemplate('home', $metatitle, $metadesc, NULL, $page_data);
    }
}
