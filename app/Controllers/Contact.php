<?php namespace App\Controllers;

class Contact extends BaseController
{
	 public $metatitle = "Simunix - Contact Us";
     public $metadesc  = "Find our address, phone number, email address or use our contact form to get in touch with us today";
		
	public function postIndex()
    {
		return $this->getIndex();
		
	}
	
    public function getIndex()
    {  

       if(isset($_POST) && isset($_POST['captcha_key'])) {
			
			$captcha_error = FALSE;
			
			$resp = $_POST['captcha_key'];						
			$captcha_error = !$this->reCaptcha_Decr($resp);	
			
			if ($captcha_error){
				echo "<h1>Captcha Error</h1>";
				echo "<p>Please <a href=\"".base_url("Contact")."\">try again</a></p>";
				die;
			}else{
				$name         = $_POST['name'];
				$organisation = $_POST['organisation'];
				$email        = $_POST['email'];
				$phone        = $_POST['phone'];
				$enquiry      = $_POST['enquiry'];

				$html_msg = "
				<h1>New general enquiry from simunix.com</h1>
				<p><strong>Enquiry Type: </strong>{$enquiry}</p>
				<p><strong>From: </strong>{$name} ({$email})</p>
				<p><strong>Organisation: </strong>{$organisation}</p>
				<p><strong>Phone: </strong>{$phone}</p>				
				";

				$this->send_authsmtp_email("Simunix.com contact form - general enquiry",$html_msg);
			}
        }
		return $this->getTemplate('contact', $this->metatitle, $this->metadesc);

    }

    
}