<?php namespace App\Controllers;

class T2a extends BaseController
{
	
	public function postIndex()
	{
		
		return $this->getIndex();
	
	}
    public function getIndex()
    {
        $pageNav = [
            "keyFeatures"   => "Key Features",
            "documentation" => "Documentation",
            "ourCustomers"  => "Our Customers",
            "contactUs"     => "Contact Us",
        ];


        if(isset($_POST) && isset($_POST['captcha_key'])) {
			
			$captcha_error = FALSE;
			
			$resp = $_POST['captcha_key'];						
			$captcha_error = !$this->reCaptcha_Decr($resp);	
			
			if ($captcha_error){
				echo "<h1>Captcha Error</h1>";
				echo "<p>Please <a href=\"".base_url("T2a")."\">try again</a></p>";
				die;
			}else{
				$name         = $_POST['first'] . " " . $_POST["last"];
				$phone        = $_POST['phone'];
				$email        = $_POST['email'];
				$enquiry      = $_POST['enquiry'];

				$html_msg = "
				<h1>New T2A enquiry from simunix.com</h1>
				<p><strong>From: </strong>{$name} ({$email})</p>
				<p><strong>Phone: </strong>{$phone}</p>
				<p><strong>Enquiry: </strong>{$enquiry}</p>
				";
	
				$this->send_authsmtp_email("Simunix.com T2A contact form - general enquiry",$html_msg);
			}
        }
        $metatitle = "Simunix - T2A - Powerful Data and Validation Services for Developers";
        $metadesc  = "T2A supplies software developers with the high quality data required for building professional websites and business applications.";

        return $this->getTemplate('t2a', $metatitle, $metadesc, $pageNav);
    }

    

}
