<?php namespace App\Controllers;


use CodeIgniter\HTTP\Response;


class Ajax extends \CodeIgniter\Controller
{
	
	
	
    public function __construct() {
		
		$this->simunixConfig = new \Config\Simunix();
		$this->myrequest = \Config\Services::request();	
		$this->callback = $this->myrequest->getGet('callback');	
	}

	public function index() {
		
		
			
		
	}
	
	private function set_output($val)
	{
		$response = service('response');

		$response->setStatusCode(Response::HTTP_OK);
		$response->setBody($val);
		$response->setHeader('Content-type', 'application/json');
		$response->noCache();

		// Sends the output to the browser
		// This is typically handled by the framework
		$response->send();
	}

	public function getreCaptcha()
	{
		$resp = $this->myrequest->getGet('response');		
		$result = $this->UM_reCaptcha($resp);
		$data['status'] = FALSE;
		if ($result)
		{
			$data['status'] = TRUE;
			$data['id'] = $result;
		}
		$this->set_output($this->callback . '(' . json_encode($data) . ');');
		
	}
	
	
	// post captcha details off to google see if the human or bot
	private function UM_reCaptcha($user_response) {

		
		
		$secret = '6LdDaQcaAAAAAMUftaOpKYxaUCMSyq1NWA4zX0y2';
		
		
		$arrContextOptions=array(
			"ssl"=>array(
				"verify_peer"=>false,
				"verify_peer_name"=>false
			)
		);  
		$url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$user_response.'&remoteip='.$this->myrequest->getIPAddress();
		//$response = do_post_request($url, $data, $headers);
		$response = file_get_contents($url,false,stream_context_create($arrContextOptions));
		
		if ($response)
		{

			$result = json_decode($response);
			
			
			
			
			if($result->success) {
				//create an encrypted time stamp
				return $this->reCaptcha_Encr();
				//return TRUE;


			}
		}

		return FALSE;
	}
	
	 

	private function reCaptcha_Encr()
	{
		
		
		
		$utc_secs = time();
		
		
		$url = $this->simunixConfig->cfg_data['xml_server'].'shopxml.aspx?cd=shopencid'
		. '&id=' . urlencode($this->myrequest->getIPAddress()."|".$utc_secs);
		
		
		log_message('critical','reCaptcha_Encr:'.$url);
		
		
		$result = simplexml_load_file($url);


		if($result && $result->valid)
		{
			return (string)$result->valid->id;
		}
		else
		{

			return FALSE;
		}
	}
	

}