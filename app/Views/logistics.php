<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <p class="leading">Simple. Powerful. Reliable</p>
            <h1>ORBIS</h1>
            <p class="trailing">From operations and dispatch to customer services and resolutions, Orbis is the go-to data solution for companies in the logistics sector.</p>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<?php echo view('pagenav', $pageNav) ?>
<div id="introduction" class="container-fluid greycontainer">
    <h2 class="heading">Providing Logistics Companies with the information they need…fast.</h2>
    <div class="row">
        <div class="col-lg-6 text-right">
            <video class="landing-video" controls poster="<?php echo base_url('img/logisticsposter.jpg') ?>" preload="true">

                <source src="<?php echo base_url('media/logistics.mp4') ?>"
                        type="video/mp4">

                Sorry, your browser doesn't support embedded videos.
            </video>
            <img class="search-type" src="" />
        </div>
        <div class="col-lg-6 content">
            <p class="headsup">How is it used?</p>
            <h2>One Powerful System</h2>
            <p>Within the logistics sector, several large companies utilise ORBIS including DHL, DPD, FEDEX and EVRi.</p>
            <p>Examples of how ORBIS is used by logistics companies include...</p>
            <ul class="gallery-links">
                <li>Verifying name and address data for residential and business deliveries.</li>
                <li>Verifying mobile phone data of customers when making contact.</li>
                <li>Utilising OS and Google map data as a visual aid when making deliveries.</li>
                <li>Verifying the age of customers when delivering age-restricted goods.</li>
                <li>Sourcing contact details of “linked people” when a delivery cannot be made.</li>
                <li>Conducting AML checks on businesses.</li>
                <li>KYC checks for new customers.</li>
            </ul>
            <div class="row contact">
                <div class="col-12">
                    <p>If you want to learn more about how ORBIS can help your company or to book a demo, please don’t hesitate to get in touch. </p>
                    <p>
                        <a class="btn btn-primary" href="<?php echo base_url('contact') ?>">Get in touch</a>
                    </p>
                </div>

            </div>
        </div>
        <div class="sepbuffer"></div>
    </div>
</div>
<div id="ourCustomers" class="container-fluid trustcontainer">
    <div class="sep"></div>
    <div class="row">
        <div class="col-12 text-center">
            <p class="leading">Trusted by 200+ companies and public sector organisations</p>
            <p class="content">ORBIS is essential to organisations of all sizes as these customers will testify…</p>
        </div>
    </div>
    <div class="row companyimages justify-content-center text-center">
        <div class="col-4 col-lg-2">
            <img class="fedexcol" src="<?php echo base_url("img/companies/logistics/fedex.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="dhlcol" src="<?php echo base_url("img/companies/logistics/dhl.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="dpdcol" src="<?php echo base_url("img/companies/logistics/dpd.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="evricol" src="<?php echo base_url("img/companies/logistics/evri.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="tntcol" src="<?php echo base_url("img/companies/logistics/tnt.png") ?>"/>
        </div>
    </div>
    <div class="sepbuffer"></div>
</div>
<div id="testimonials" class="container-fluid blackcontainer">
    <div class="sep"></div>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-3 text-right">
            <p class="headsup">Logistics Companies</p>
            <p class="big-text">Verify customers, aid deliveries</p>
        </div>
        <div class="col-12 col-lg-3 text-left">
            <p class="right-title">Business Relationship Manager</p>
            <p class="right-subtitle">DPD</p>
            <p class="right-content">&ldquo;We use ORBIS to for many reasons cross departmentally. Generally we access the database to run customer checks to ensure parcels are not getting delivered to empty properties and the customer onboarding checks are done.
                ORBIS also supports our mapping, by providing clean address data to avoid any delivery issues.&rdquo;</p>
            <p class="seppara"><a class="btn btn-primary" href="<?php echo base_url('contact' ) ?>" role="button">Contact Us</a></p>
        </div>
    </div>
</div>
<div id="useCases"></div>
<div id="simunix-slider">
    <p class="subtitle">What can we do?</p>
    <h2>How our customers are using ORBIS</h2>
    <div class="gallery-slider">
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/logistics/vna.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Verifying Name &amp; Address Data</p>
            <p class="slider-content">Ensure delivery is going to the people listed as living at the address for delivery. Validating the information of the recipients reduces losses with regards to wasted delivery time and potential fraud.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/logistics/vmpd.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Verifying Mobile Phone Data of Customers</p>
            <p class="slider-content">Validate a mobile phone is current and accurate to ensure contact with a customer is possible. Check to see if a fraudulent mobile number is being used.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/logistics/os.png") ?>" width="328" height="328" />
            <p class="slider-title">Make Deliveries Using Ordnance Survey Maps</p>
            <p class="slider-content">Use Ordnance Survey map data as a visual aid when making deliveries. Get pinpoint accurate address details and visuals by viewing the information in Ordnance Survey map tiles. Improve logistics by utilising URPN’s on delivery routes.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/logistics/vadrd.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Verify Age For Delivery of Restricted Goods</p>
            <p class="slider-content">Using data to check a recipient’s age before dispatching a parcel can reduce delays in drop off times. This further protects the delivery company when used in conjunction with door step age checks.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/logistics/vnd.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Validate Neighbour Details</p>
            <p class="slider-content">Source contact details of ‘linked people’ when a delivery cannot be made.</p>
            <p class="slider-content">Validate neighbour details when fulfilling a ‘leave with neighbour’ delivery request. This measure can protect against fraudulent parcel management.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/logistics/aml.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Conduct AML Checks on Businesses</p>
            <p class="slider-content">Validate company information and conduct credit checks when creating large customer accounts or when bulk deliveries are being made. Utilising Bank Account Validation can ensure payments are being made correctly.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/logistics/kyc.jpg") ?>" width="328" height="328" />
            <p class="slider-title">KYC Check for New Customers</p>
            <p class="slider-content">Validate company information and conduct credit checks when creating large customer accounts or when bulk deliveries are being made. Utilising Bank Account Validation can ensure payments are being made correctly.</p>
        </div>
    </div>
</div>
<div id="pricing" class="container-fluid bluecontainer">
    <div class="sep"></div>
    <?php if(isset($_GET['m'])): ?>
        <p class="thankyou-message">Thank you for getting in touch. A member of our team will respond to your query as soon as possible</p>
    <?php else: ?>
    <div class="container">
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <h2>Packages that work for your business</h2>
            </div>
        </div>
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <p>Contact our Sales Experts to set up the right pricing for your business needs.</p>
            </div>
        </div>
        <form method="post" action="<?php echo base_url('orbis?m=thank_you'); ?>"  class="cpta_enabled_form">
		<input type="hidden" id="captcha_key" name="captcha_key"  />
            <div class="row">
                <div class="offset-lg-3 col-lg-3">
                    <div class="form-group">
                        <input name="first" type="text" class="form-control" placeholder="First name" required/>
                    </div>
					<div class="form-group">
                        <input type="text" name="last" class="form-control" placeholder="Last name" required/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="company" class="form-control" placeholder="Company" required/>
                    </div>
                    
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <input type="tel" name="phone" class="form-control" placeholder="Phone Number" required/>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" required/>
                    </div>
                    <div class="form-group">
                        <select name="enquiry" class="form-control">
                            <option value="0" selected disabled>Feature you're interested in</option>
                            <option value="orbis">ORBIS</option>
                            <option value="orbis-plus">ORBIS+</option>
                            <option value="orbis-network">ORBIS Network</option>
                        </select>
                    </div>
                    <div class="caret"><i class="far fa-angle-down"></i></div>
                </div>
            </div>
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <p><input type="submit" class="btn btn-primary" value="Send my request"/></p>
                </div>
            </div>
        </form>
    </div>
    <?php endif; ?>
</div>


 <div class="g-recaptcha" 
   data-sitekey="6LdDaQcaAAAAAF2WEbDYAKe7Q9Jucsqg_pjbyzrM"
   data-size="invisible"
   data-callback="formSubmit">
</div>

<script src="https://www.google.com/recaptcha/api.js"></script>