<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="container-fluid">
        <div class="row mainrow">
            <div class="col-12 offset-lg-1 col-lg-4">
                <h1>Search <br class="mobile-break"/><span id="searchTypewriter"></span></h1>
                <p class="lead people">Our people finder is used by virtually every
                    business and public sector type. We offer the UK’s
                    most powerful search tool to help you find people
                    and ‘linked’ individuals.</p>
                <p class="lead businesses">Our business and director searches use the latest
                    Companies House data to provide a wealth of information on UK companies
                    including director details, credit reports, accounts, and company filings</p>
                <p class="lead addresses">Find complete addresses plus details on the property
                    and occupants. Our address search uses the most up-to-date data, which is why
                    so many companies use us to clean their customer information.</p>
                <a class="btn btn-primary" href="<?php echo base_url('orbis' ) ?>" role="button">Find out more</a>
            </div>
            <div class="col-0 offset-lg-1 col-lg-2">
                <div class="pin-container pin1">
                    <div class="pin-icon people"><img src="js/glitchdrop/img/people.png" /></div>
                    <div class="pin">
                    </div>
                    <div class="pin-text">
                        <p class="glitch glitchhead" data-text="120M"></p>
                        <p class="glitch glitchsub" data-text="People records"></p>
                    </div>
                </div>
            </div>
            <div class="col-0 col-lg-2">
                <div class="pin-container pin2">
                    <div class="pin-icon company"><img src="js/glitchdrop/img/company.png" /></div>
                    <div class="pin">
                    </div>
                    <div class="pin-text">
                        <p class="glitch glitchhead" data-text="3.5M"></p>
                        <p class="glitch glitchsub" data-text="Business records"></p>
                    </div>
                </div>
            </div>
            <div class="col-0 col-lg-2">
                <div class="pin-container pin3">
                    <div class="pin-icon address"><img src="js/glitchdrop/img/address.png" /></div>
                    <div class="pin">
                    </div>
                    <div class="pin-text">
                        <p class="glitch glitchhead" data-text="30M+"></p>
                        <p class="glitch glitchsub" data-text="Address records"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center companiesrow">
            <div class="col-12 col-lg-5 text-center">
                <p>Trusted by 200+ companies and public sector organisations</p>
            </div>
        </div>
        <div class="row companyimages justify-content-center text-center">
            <div class="col-4 col-lg-2">
                <img class="tata" src="img/companies/tata.png" />
            </div>
            <div class="col-0 col-lg-2">
                <img class="dhl" src="img/companies/dhl.png" />
            </div>
            <div class="col-4 col-lg-2">
                <img class="dpd" src="img/companies/dpd.png" />
            </div>
            <div class="col-4 col-lg-2">
                <img class="hsbc" src="img/companies/hsbc.png" />
            </div>
            <div class="col-0 col-lg-2">
                <img class="tnt" src="img/companies/tnt.png" />
            </div>
        </div>
    </div>
    <div class="sepbuffer"></div>
</div>
<div class="container">
    <div class="sep"></div>
    <div class="row justify-content-center text-center">
        <div class="col-12 col-lg-8">
            <h2>People &amp; business data made accessible
                for search and analysis</h2>
        </div>
    </div>
    <div class="row justify-content-center text-center">
        <div class="col-12 col-lg-6">
            <p class="squareslider-desc">At Simunix we bring together people and business information from
                a variety of government, commercial and opted-in marketing
                sources and make it available via a platform to suit you.</p>
        </div>
    </div>
    <div class="row justify-content-center text-center">
        <div class="col-12 col-lg-8">
            <div class="squareSlide">
                <span class="primary pri1">▣</span>
                <span class="pri-header pri1-header">ORBIS</span>
                <div class="group1">
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                </div>
                <span class="primary pri2">▣</span>
                <span class="pri-header pri2-header">UKPHONEBOOK.COM</span>
                <div class="group2">
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                    <span>■</span>
                </div>
                <span class="primary pri3">▣</span>
                <span class="pri-header pri3-header">T2A</span>
            </div>
            <div class="imgGroup imgGroup1"><img src="img/tempgroup1.png" /></div>
            <div class="imgGroup imgGroup2"><img src="img/tempgroup2.png" /></div>
            <div class="imgGroup imgGroup3"><img src="img/tempgroup3.png" /></div>
            <div class="imgGroupMobile"><img src="img/tempgroupmobile.png" /></div>
            <p class="seppara"><a class="btn btn-primary" href="<?php echo base_url('contact' ) ?>" role="button">Request a demo</a></p>
        </div>
    </div>
    <div class="sepbuffer withpara"></div>
</div>

<div class="container-fluid section-accreditation">
	<div class="container">
		
		<center>
		<h2>Our accreditations</h2>
		
		<p>Simunix is proud to have achieved the following accreditations:</p>
		
		<div>
			<div class="accred accred1">&nbsp;</div>
			<div class="accred accred2">&nbsp;</div>
			<div class="accred accred3">&nbsp;</div>
			<div class="accred accred4">&nbsp;</div>
			<div class="accred accred5">&nbsp;</div>
		</div>
		</center>
		
		
	</div>
	 <div class="sepbuffer withpara"></div>
</div>

<div class="container-fluid greycontainer">
    <div class="sep"></div>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 text-center">
            <h2>Powerful search tools designed for you</h2>
            <div class="slider">
                <div>
                    <img src="img/slider/address.png" />
                    <p class="slide-head">See who lives at an address</p>
                    <p class="slide-content">Obtain the names and telephone numbers of all residents who have lived, or are currently living, at a specified address.</p>
                </div>
                <div>
                    <img src="img/slider/age.png" />
                    <p class="slide-head">Age verification</p>
                    <p class="slide-content">Validate your data against our UK people data sources to see if a record, aged over 18, is found.</p>
                </div>
                <div>
                    <img src="img/slider/telephone.png" />
                    <p class="slide-head">Find a business telephone number</p>
                    <p class="slide-content">Search current UK telephone data to find UK business telephone numbers by name and/or location.</p>
                </div>
                <div>
                    <img src="img/slider/geo.png" />
                    <p class="slide-head">Find a business telephone number</p>
                    <p class="slide-content">Search current UK telephone data to find UK business telephone numbers by name and/or location.</p>
                </div>
                <div>
                    <img src="img/slider/address.png" />
                    <p class="slide-head">Find a Person</p>
                    <p class="slide-content">Search for a UK individual across our vast dataset of edited electoral roll, phone book and consumer data.</p>
                </div>
                <div>
                    <img src="img/slider/age.png" />
                    <p class="slide-head">Verify that a UK person exists</p>
                    <p class="slide-content">Check that a UK individual, such as a potential customer, is who they say they are.</p>
                </div>
                <div>
                    <img src="img/slider/telephone.png" />
                    <p class="slide-head">See who lives at an address</p>
                    <p class="slide-content">Find the names and telephone numbers of all current and previous occupants of a specified address.</p>
                </div>
                <div>
                    <img src="img/slider/address.png" />
                    <p class="slide-head">Find a telephone number</p>
                    <p class="slide-content">Search for UK residential and business telephone numbers by name and/or location.</p>
                </div>
                <div>
                    <img src="img/slider/age.png" />
                    <p class="slide-head">Check a number against the TPS</p>
                    <p class="slide-content">Make sure you can legally call someone by checking your customers against the Telephone Preference Service.</p>
                </div>
                <div>
                    <img src="img/slider/telephone.png" />
                    <p class="slide-head">Age verification</p>
                    <p class="slide-content">Validate that someone is aged 18+ if you are selling age restricted products or services.</p>
                </div>
                <div>
                    <img src="img/slider/geo.png" />
                    <p class="slide-head">Search for a company</p>
                    <p class="slide-content">Find credit reports and other information on UK companies from Companies House data.</p>
                </div>
                <div>
                    <img src="img/slider/geo.png" />
                    <p class="slide-head">Search for a company</p>
                    <p class="slide-content">Find a person’s current and previous directorships of UK companies.</p>
                </div>
            </div>

            <!-- googleoff:index -->
            <div class="slider-mobile">
                <div>
                    <img src="img/slider/address.png" />
                    <p class="slide-head">See who lives at an address</p>
                    <p class="slide-content">Obtain the names and telephone numbers of all residents who have lived, or are currently living, at a specified address.</p>
                </div>
                <div>
                    <img src="img/slider/age.png" />
                    <p class="slide-head">Age verification</p>
                    <p class="slide-content">Validate your data against our UK people data sources to see if a record, aged over 18, is found.</p>
                </div>
                <div>
                    <img src="img/slider/telephone.png" />
                    <p class="slide-head">Find a business telephone number</p>
                    <p class="slide-content">Search current UK telephone data to find UK business telephone numbers by name and/or location.</p>
                </div>
                <div>
                    <img src="img/slider/geo.png" />
                    <p class="slide-head">Find a business telephone number</p>
                    <p class="slide-content">Search current UK telephone data to find UK business telephone numbers by name and/or location.</p>
                </div>
                <div>
                    <img src="img/slider/address.png" />
                    <p class="slide-head">Find a Person</p>
                    <p class="slide-content">Search for a UK individual across our vast dataset of edited electoral roll, phone book and consumer data.</p>
                </div>
                <div>
                    <img src="img/slider/age.png" />
                    <p class="slide-head">Verify that a UK person exists</p>
                    <p class="slide-content">Check that a UK individual, such as a potential customer, is who they say they are.</p>
                </div>
                <div>
                    <img src="img/slider/telephone.png" />
                    <p class="slide-head">See who lives at an address</p>
                    <p class="slide-content">Find the names and telephone numbers of all current and previous occupants of a specified address.</p>
                </div>
                <div>
                    <img src="img/slider/address.png" />
                    <p class="slide-head">Find a telephone number</p>
                    <p class="slide-content">Search for UK residential and business telephone numbers by name and/or location.</p>
                </div>
                <div>
                    <img src="img/slider/age.png" />
                    <p class="slide-head">Check a number against the TPS</p>
                    <p class="slide-content">Make sure you can legally call someone by checking your customers against the Telephone Preference Service.</p>
                </div>
                <div>
                    <img src="img/slider/telephone.png" />
                    <p class="slide-head">Age verification</p>
                    <p class="slide-content">Validate that someone is aged 18+ if you are selling age restricted products or services.</p>
                </div>
                <div>
                    <img src="img/slider/geo.png" />
                    <p class="slide-head">Search for a company</p>
                    <p class="slide-content">Find credit reports and other information on UK companies from Companies House data.</p>
                </div>
                <div>
                    <img src="img/slider/geo.png" />
                    <p class="slide-head">Search for a company</p>
                    <p class="slide-content">Find a person’s current and previous directorships of UK companies.</p>
                </div>
            </div>
            <!-- googleon: index -->

            <p class="seppara text-center"><a class="btn btn-primary" href="<?php echo base_url('contact' ) ?>" role="button">Contact Us</a></p>
        </div>
    </div>
    <div class="sepbuffer withpara"></div>
</div>
<div id="simunix-gallery" class="home-gallery">
    <div class="sep"></div>
    <div id="simunix-gallery-left">
        <div id="gallery-left-container">
            <div id="gallery-left">
                <div id="gallery-left-content1">
                    <img class="gallery-left1-developers" src="<?php echo base_url('img/gallery/home/developers3.png') ?>">
                    <img class="gallery-left1-consumers" src="<?php echo base_url('img/gallery/home/consumers3.png') ?>">
                    <img class="gallery-left1-corporate" src="<?php echo base_url('img/gallery/home/corporate3.png') ?>">
                </div>
                <div id="gallery-left-content2">
                    <img class="gallery-left2-corporate" src="<?php echo base_url('img/who/corporate2.png') ?>">
                    <img class="gallery-left2-developers" src="<?php echo base_url('img/who/developers2.png') ?>">
                    <img class="gallery-left2-consumers" src="<?php echo base_url('img/who/consumers2.png') ?>">
                </div>
                <div id="gallery-left-content3">
                    <div class="gallery-link">
                        <a href="#" class="next corp">&#8594;</a>
                        <div class="gallery-linktext">
                            <span class="nexttext">next</span>
                            <span class="nexttextlabel">Organisations</span>
                        </div>

                    </div>
                </div>
            </div>
            <div id="gallery-right">
                <div id="gallery-right-content">
                    <img class="gallery-right-consumers" src="<?php echo base_url('img/gallery/home/consumers1.png') ?>" />
                    <img class="gallery-right-corporate" src="<?php echo base_url('img/gallery/home/corporate1.png') ?>" />
                    <img class="gallery-right-developers" src="<?php echo base_url('img/gallery/home/developers1.png') ?>" />
                </div>
            </div>
        </div>
    </div>
    <div id="gallery-right-container">
        <p class="headsup">For who?</p>
        <h2 class="gallery-heading">Consumers</h2>
        <p class="gallery-subheading-consumers">Use ukphonebook.com to look up telephone and address information for people and businesses.</p>
        <p class="gallery-subheading-corporate">Harness the investigative power of ORBIS to find information on people, addresses and companies in the UK.</p>
        <p class="gallery-subheading-developers">Open up your website, CRM, or app to a vast amount of data on people, addresses and companies with our T2A API.</p>
        <div class="gallery-grid">
            <div class="gallery-grid-item">
                <div class="bluesquare">
                    <img src="<?php echo base_url('img/bluesquare.png'); ?>">
                </div>
                <p class="grid-item1-consumers">Address &amp; Postcode Lookup - Use our postcode and address finder to find up-to-date and accurate address information.</p>
                <p class="grid-item1-corporate">Person search – find an individual and ‘linked’ people from searching across multiple data sources.</p>
                <p class="grid-item1-developers">Address/postcode lookup – auto-complete address details during customer registration, ensuring accurate data.</p>
            </div>
            <div class="gallery-grid-item">
                <div class="bluesquare">
                    <img src="<?php echo base_url('img/bluesquare.png'); ?>">
                </div>
                <p class="grid-item2-consumers">Phone Search - Find UK residential and business phone numbers.</p>
                <p class="grid-item2-corporate">Search for a company – find a business and view Companies House details about the company and it’s directors.</p>
                <p class="grid-item2-developers">Validate a person – check your customer exists, and they are aged 18+ if you’re selling age restricted products.</p>
            </div>
            <div class="gallery-grid-item">
                <div class="bluesquare">
                    <img src="<?php echo base_url('img/bluesquare.png'); ?>">
                </div>
                <p class="grid-item3-consumers">Find a Person - Search the edited UK electoral register, the phone book, and consumer data.</p>
                <p class="grid-item3-corporate">Person verification – check your customer exists, and they are aged 18+ if you’re selling age restricted products.</p>
                <p class="grid-item3-developers">Find company details – look up information about a business and it’s directors before engaging in business with them.</p>
            </div>
            <div class="gallery-grid-item">
                <div class="bluesquare">
                    <img src="<?php echo base_url('img/bluesquare.png'); ?>">
                </div>
                <p class="grid-item4-consumers">Company Search - Search the Companies House database, including every limited company based or with an office in the UK.</p>
                <p class="grid-item4-corporate">Customer database – clean your data against PAF and TPS. Add phone numbers with our Teleappending service.</p>
                <p class="grid-item4-developers">Customer database – Append telephone numbers and check against TPS and Bereavement register before calling.</p>
            </div>
        </div>
        <div class="gallery-link-mobile">
            <a href="#" class="next corp">&#8594;</a>
            <div class="gallery-linktext">
                <span class="nexttext">next</span>
                <span class="nexttextlabel">Organisations</span>
            </div>
        </div>
    </div>
</div>

    <div class="sepbuffer"></div>
<div class="container-fluid blackcontainer">
    <div class="sep"></div>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-3 text-right">
            <p class="headsup">Police &amp; Security</p>
            <p class="big-text">Real-time data that moves with you</p>
        </div>
        <div class="col-12 col-lg-3 text-left">
            <p class="right-title">Chief Inspector</p>
            <p class="right-subtitle">West Midlands Police</p>
            <p class="right-content">&ldquo;We regularly
                use ORBIS to locate and contact individuals of interest to historic investigations&rdquo;</p>
            <p class="seppara"><a class="btn btn-primary" href="<?php echo base_url('contact' ) ?>" role="button">Contact Us</a></p>
        </div>
    </div>
</div>
<div class="container-fluid dottycontainer">
    <div class="container">
        <div class="sep"></div>
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 text-center">
                <h2>Supporting GDPR</h2>
                <p>We are committed to ensuring that individuals’ personal data displayed and collected on our websites is compliant with all applicable data protection legislation, such as GDPR. We work closely with our data partners to ensure that we only use consented or ‘opted-in’ data. Please see our <a href="<?php echo base_url('privacy' ) ?>">privacy policy</a> for more information. </p>
                <p><i class="fa fa-chevron-right" aria-hidden="true"></i>
                    <a href="<?php echo base_url('contact' ) ?>">Contact Us</a></p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid bluecontainer">
    <div class="row justify-content-center">
        <p><a class="btn btn-primary" href="<?php echo base_url('contact' ) ?>">Contact us</a> <a class="btn btn-secondary" href="<?php echo base_url('about' ) ?>">Find out more</a></p>
    </div>
</div>