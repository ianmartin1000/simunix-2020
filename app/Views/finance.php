<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <p class="leading">Simple. Powerful. Reliable</p>
            <h1>ORBIS</h1>
            <p class="trailing">Orbis is the go-to data solution for companies in the financial sector</p>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<?php echo view('pagenav', $pageNav) ?>
<div id="introduction" class="container-fluid greycontainer">
    <h2 class="heading">Providing the Financial Sector with the information it needs…fast.</h2>
    <div class="row">
        <div class="col-lg-5 sourcesinfograph text-right">
            <img class="infographic" src="<?php echo base_url('img/infographic.png')?>" />
        </div>
        <div class="col-lg-6 offset-lg-1 content">
            <p class="headsup">How is it used?</p>
            <h2>One Powerful System</h2>
            <p>In the financial sector, ORBIS is used cross-departmentally by banks and building societies, from financial crime and fraud investigations to remediation and credit analytics.</p>
            <p>Below are a few examples of how ORBIS is used by banks and building societies.</p>
            <ul class="gallery-links">
                <li>KYC/AML checking on people and businesses</li>
                <li>Intelligence gathering for fraud and financial crime investigations</li>
                <li>Remediation work and data cleansing</li>
                <li>Verifying current and historic data for mortgages, loans, and account applications</li>
                <li>Estate and asset validation (via Land Registry and The Bereavement Register data)</li>
                <li>Skip tracing (via historic data linking)</li>
            </ul>
            <div class="row contact">
                <div class="col-12">
                    <p>For more information on the uses of ORBIS in the financial sector, take a look at our brochure here.</p>
                    <p>If you are interested in booking an ORBIS demo, please get in touch.</p>

                    <p>
                        <a class="btn btn-primary" href="<?php echo base_url('contact') ?>">Get in touch</a>
                    </p>
                </div>

            </div>
        </div>
        <div class="sepbuffer"></div>
    </div>
</div>
<div id="ourCustomers" class="container-fluid trustcontainer">
    <div class="sep"></div>
    <div class="row">
        <div class="col-12 text-center">
            <p class="leading">Trusted by 200+ customers who rely on ORBIS for their information needs</p>
            <p class="content">ORBIS is essential to organisations of all sizes as these customers will testify…</p>
        </div>
    </div>
    <div class="row companyimages justify-content-center text-center">
        <div class="col-4 col-lg-2">
            <img class="bakertillycol" src="<?php echo base_url("img/companies/bakertillycol.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="hsbccol" src="<?php echo base_url("img/companies/hsbccol.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="amexcol" src="<?php echo base_url("img/companies/amexcol.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="yorkbuilcol" src="<?php echo base_url("img/companies/yorkbuilcol.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="tntcol" src="<?php echo base_url("img/companies/tntcol.png") ?>"/>
        </div>
    </div>
    <div class="sepbuffer"></div>
</div>
<div id="testimonials" class="container-fluid blackcontainer">
    <div class="sep"></div>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-3 text-right">
            <p class="headsup">Financial Sector</p>
            <p class="big-text">Search and verification</p>
        </div>
        <div class="col-12 col-lg-3 text-left">
            <p class="right-title">Financial Crime Manager</p>
            <p class="right-subtitle">Financial Sector</p>
            <p class="right-content">&ldquo;ORBIS is used in the financial sector across-the-board. Both the residential and business data available in ORBIS is valuable with regards to the search and verification needs of users. From retrieving data for fraud and financial crime investigations to onboarding and remediation work, ORBIS is useful for those in all departments in banks and building societies.&rdquo;</p>
            <p class="seppara"><a class="btn btn-primary" href="<?php echo base_url('contact' ) ?>" role="button">Contact Us</a></p>
        </div>
    </div>
</div>
<div id="useCases"></div>
<div id="simunix-slider">
    <p class="subtitle">What can we do?</p>
    <h2>How our customers are using ORBIS</h2>
    <div class="gallery-slider">
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/finance/st.png") ?>" width="328" height="328" />
            <p class="slider-title">Skip Tracing</p>
            <p class="slider-content">The historic linking capabilities in ORBIS allows users to trace an individual from a previous home to their current address. This data is utilised in the financial sector for skip tracing as well as sourcing current name, address and contact details.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/finance/kyc.png") ?>" width="328" height="328" />
            <p class="slider-title">KYC/AML Checking</p>
            <p class="slider-content">The verification functions available in ORBIS allow users to conduct a variety of checks against people and businesses to mitigate fraud and ensure successful customer onboarding.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/finance/ve.png") ?>" width="328" height="328" />
            <p class="slider-title">Verify Current and Historic Data</p>
            <p class="slider-content">ORBIS facilitates address linking and verification via historic and current data sets. These methods are often used to validate the ID of individuals applying for loans, mortgages and bank accounts.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/finance/va.png") ?>" width="328" height="328" />
            <p class="slider-title">Estate and Asset Validation</p>
            <p class="slider-content">Bereavement teams often use the Land Registry and Deceased Check functions in ORBIS when undertaking estate validation work. This data allows users to deduce the ownership status of assets and whether loans should be revoked based on deceased status.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/finance/dc.png") ?>" width="328" height="328" />
            <p class="slider-title">Remediation Work and Data Cleansing</p>
            <p class="slider-content">You can enhance and cleanse data sets in ORBIS via our Data Enrichment function. Save time when conducting remediation work and append valuable data via our bulk data extraction capabilities.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/finance/ig.png") ?>" width="328" height="328" />
            <p class="slider-title">Intelligence Gathering</p>
            <p class="slider-content">ORBIS provides an array or search and verification functions that allows users to investigate people and business potentially involved in fraud and financial crime. This includes tracing via address linking to locate those potentially committing fraud.</p>
        </div>
    </div>
</div>
<div id="pricing" class="container-fluid bluecontainer">
    <div class="sep"></div>
    <?php if(isset($_GET['m'])): ?>
        <p class="thankyou-message">Thank you for getting in touch. A member of our team will respond to your query as soon as possible</p>
    <?php else: ?>
    <div class="container">
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <h2>Packages that work for your business</h2>
            </div>
        </div>
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <p>Contact our Sales Experts to set up the right pricing for your business needs.</p>
            </div>
        </div>
        <form method="post" action="<?php echo base_url('orbis?m=thank_you'); ?>"  class="cpta_enabled_form">
		<input type="hidden" id="captcha_key" name="captcha_key"  />
            <div class="row">
                <div class="offset-lg-3 col-lg-3">
                    <div class="form-group">
                        <input name="first" type="text" class="form-control" placeholder="First name" required/>
                    </div>
					<div class="form-group">
                        <input type="text" name="last" class="form-control" placeholder="Last name" required/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="company" class="form-control" placeholder="Company" required/>
                    </div>
                    
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <input type="tel" name="phone" class="form-control" placeholder="Phone Number" required/>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" required/>
                    </div>
                    <div class="form-group">
                        <select name="enquiry" class="form-control">
                            <option value="0" selected disabled>Feature you're interested in</option>
                            <option value="orbis">ORBIS</option>
                            <option value="orbis-plus">ORBIS+</option>
                            <option value="orbis-network">ORBIS Network</option>
                        </select>
                    </div>
                    <div class="caret"><i class="far fa-angle-down"></i></div>
                </div>
            </div>
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <p><input type="submit" class="btn btn-primary" value="Send my request"/></p>
                </div>
            </div>
        </form>
    </div>
    <?php endif; ?>
</div>


 <div class="g-recaptcha" 
   data-sitekey="6LdDaQcaAAAAAF2WEbDYAKe7Q9Jucsqg_pjbyzrM"
   data-size="invisible"
   data-callback="formSubmit">
</div>

<script src="https://www.google.com/recaptcha/api.js"></script>