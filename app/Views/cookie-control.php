
	
	<script src="https://cc.cdn.civiccomputing.com/9/cookieControl-9.x.min.js"></script>
	<script>
	
			var apimode = '<?php echo config('Simunix')->cfg_data['shop_api_mode']; ?>';
			<?php if(config('Simunix')->cfg_data['shop_api_mode'] == "P"): //we only do analytics on live enviroment ?>
			var gtags_exist = true;
			<?php else: ?>
			var gtags_exist = false;
			<?php endif; ?>
			
			var config = {
			"apiKey": "b5569108a7ef3c00c4319f73588cea1ca01df712",
			"product": "PRO",
			"setInnerHTML": true,
			"wrapInnerHTML": true,
			"text": {
				"acceptSettings": 'Allow all',
				"accept" : 'Allow all',
				"title": "<h2>This website uses cookies</h2>",
				"intro": "<p>These cookies analyse website traffic and customise advertisements.</p>",
				"necessaryTitle": "<h3>Strictly Necessary Cookies</h3>",
				"necessaryDescription": "<p>Strictly necessary cookies are required to enable the basic features of this site, such as form Captchas. </p>",
				"cornerButton": "Cookie Control",
				"notifyTitle": "<h2>This website uses cookies</h2>",
				"notifyDescription": "<p>These cookies analyse website traffic and customise advertisements.</p>"
				
			},
			
			"optionalCookies": [
			/*{
				"name": "functional",
				"label": "<h3>Preference Cookies</h3>",
				"description": "<p>Preference cookies help improve your experience on the site. E.G. remembering which features you don't want to receive cost notifications for.</p>",
				"cookies": ["dontshowagain", "return_url", "bademail_read","records_per_page"],
				"onAccept": "function(){}",
				"onRevoke": "function(){}"
			},*/
			{
				"name": "analytics",
				"label": "<h3>Analytical Cookies</h3>",
				"description": "<p>Analytical cookies help us to improve our website by collecting and reporting information on its usage.</p>",
				"cookies": ["_ga", "_ga_*", "_gid", "_gat", "_dc_gtm_", "AMP_TOKEN", "_gat_*", "_gac_", "__utma", "__utmt", "__utmb", "__utmc", "__utmz", "__utmv", "__utmx", "__utmxx", "FPAU", "FPID", "FPLC"],
				
				
				onAccept: function () {
					//console.log('analytics-onAccept');
					
					if (gtags_exist){ //not applicable in dev	, toggle consent mode					
						//GA4
						gtag('consent', 'update', {
							 'analytics_storage': 'granted'
							});
					}
				},
				onRevoke: function () {
					//console.log('analytics-onRevoke');
					
					if (gtags_exist){ //not applicable in dev	, toggle consent mode					
						//GA4
						gtag('consent', 'update', {
							'analytics_storage': 'denied'
						});
					}
				}
			}, {
				"name": "marketing",
				"label": "<h3>Targeting Cookies</h3>",
				"description": "<p>We use targeting cookies to help us improve the relevancy of advertising campaigns you receive.</p>",
				"cookies": ["lidc", "bcookie", "bscookie", "trkCode", "trkInfo", "li_oatml", "liap", "lissc", "spectroscopyId", "UserMatchHistory", "lang", "li_gc", "li_rm", "AnalyticsSyncHistory", "ln_or", "li_sugr"],
				

				onAccept: function () {
					//console.log('marketing-onAccept');
					if (gtags_exist){ //not applicable in dev, toggle consent mode					
						
						gtag('consent', 'update', {
							'ad_storage': 'granted',
							'ad_user_data': 'granted',
							'ad_personalization': 'granted'
						});
					}
				},
				onRevoke: function () {
					//console.log('marketing-onRevoke');
					
					if (gtags_exist){ //not applicable in dev	, toggle consent mode					
						
						
						//google ads
						gtag('consent', 'update', {
						  'ad_storage': 'denied',
						  'ad_user_data': 'denied',
						  'ad_personalization': 'denied'
						});
					}
				}
			}],
			"rejectButton": false,
			"necessaryCookies": ["_GRECAPTCHA"],
			
			"branding": {
				"removeIcon": true,
				"removeAbout": true,
				"fontFamily": "open-sans, sans-serif",
				"fontSizeTitle": "1.4em",
				"fontSizeHeaders": "1.3em",
				"fontSize": "1.25em",
				"fontColor": "#000",
				"backgroundColor": "#F4F4F4",
				"acceptText": "#fff",
				"acceptBackground": "#0953FF",
				"toggleText": "#FFFFFF",
				"rejectText": "#000",
				"alertText": "#000",
				"toggleBackground": "#000",
				"toggleColor": "#fff"
			},
			"notifyOnce": false,
			"initialState": "box",
			"theme": "light",
			"layout": "popup",
			"statement" : {
			"description":'For more information',
			"name":'see our cookie policy.',
			"url": 'https://simunix.com/privacy',
			"updated" : '10/07/2024'
			}
		};

		CookieControl.load( config );	
	</script>
	
	