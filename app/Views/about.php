<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <h1>About Simunix</h1>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<div class="container firstcontainer">
    <div class="row">
        <div class="col-lg-4 offset-lg-2">
            <p class="headsup">History</p>
            <h2>Simunix</h2>
            <p>Established in 1998, Simunix was the first non-telecom company to offer a free online telephone directory as <a href="<?php echo base_url('ukpb' ) ?>">ukphonebook.com</a>, following the deregulation of the directory enquiry services. Since then, <a href="<?php echo base_url('ukpb' ) ?>">ukphonebook.com</a> and our  directory enquiries phone number, <a href="https://118365.co.uk">118 365</a>, have become the go to source for people, business and address searching in the UK.</p>
            <p>While <a href="<?php echo base_url('ukpb' ) ?>">ukphonebook.com</a> still offers a limited number of free searches to UK consumers, Simunix created a fully corporate version of the service incorporating many more data sources and search functions under the brand name, <a href="<?php echo base_url('orbis' ) ?>">ORBIS</a>. This search tool is now used by businesses and public sector organisations of all sizes and sectors:</p>
        </div>
        <div class="col-lg-5 text-center">
            <img class="aboutimg" src="<?php echo base_url('/img/about.png') ?>" />
        </div>
    </div>
    <div class="row grids">
        <div class="col-lg-2 offset-lg-2">
            <ul>
                <li><p>Police</p></li>
                <li><p>Fire &amp; Rescue</p></li>
                <li><p>NHS</p></li>
            </ul>
        </div>
        <div class="col-lg-2">
            <ul>
                <li><p>Banking</p></li>
                <li><p>Legal Services</p></li>
                <li><p>Debt Collection</p></li>
            </ul>
        </div>
        <div class="col-lg-2">
            <ul>
                <li><p>Property</p></li>
                <li><p>Financial Services</p></li>
                <li><p>Sales &amp; Marketing</p></li>
            </ul>
        </div>
        <div class="col-lg-2">
            <ul>
                <li><p>Journalism</p></li>
                <li><p>Broadcast &amp; Media</p></li>
                <li><p>Logistics</p></li>
            </ul>
        </div>
    </div>
    <div class="sepbuffer"></div>
</div>
<div id="careers" class="container-fluid greycontainer">
    <div class="sep"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-2">
                <p class="headsup">Team</p>
                <h2>Join Us</h2>
                <p>We are always looking for talented individuals with programming experience. Required disciplines are .NET, PHP, MySQL, CSS, Javascript etc.</p>
                <p>We are also keen to talk to people with sales experience in the following areas; media, public sector, software licensing, IT systems.</p>
                <p>Please send your CV with a covering letter to <a class="mailto" href="mailto:careers@simunix.com">careers@simunix.com</a></p>
                <h3>Current Vacancies</h3>
                <div class="vacancies">
                    <p>We currently have no specific vacancies</p>
                </div>
            </div>
            <div class="col-lg-5 text-center">
                <img class="joinusimg" src="<?php echo base_url('/img/joinus.png') ?>" />
            </div>
        </div>
    </div>
</div>
