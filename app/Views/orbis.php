<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <p class="leading">Simple. Powerful. Reliable</p>
            <h1>ORBIS</h1>
            <p class="trailing">People and business searching at your fingertips. Used by businesses and organisations of all sizes, Orbis is the UK’s premier people and business search tool.</p>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<?php echo view('pagenav', $pageNav) ?>
<div id="keyFeatures" class="container-fluid whitecontainer">
    <div class="container">
        <div class="row desktop-only">
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/phone.png') ?>" />
                <p class="grid-head">Phone Search</p>
                <p class="grid-content">With 118 services costing as much as £20 per call, a low-cost alternative makes sense! Our phone number search gives you and your colleagues unlimited access to exactly the same directory data derived from all the UK’s telecom operators that drives the expensive voice services.</p>
            </div>
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/address.png') ?>" />
                <p class="grid-head">Address &amp; Postcode Lookup</p>
                <p class="grid-content">Royal Mail and Ordnance Survey between them have every addressable location in the UK listed. ORBIS gives you direct access via a simple search tool to this important resource. If your addressing needs are even greater, you can access this data via our powerful API. Just contact us for details.</p>
            </div>
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/person.png') ?>" />
                <p class="grid-head">Find a Person</p>
                <p class="grid-content">Our people finder is used by virtually every business and public sector type. Drawing on multiple resources and opted-in data sets, we offer what is probably the UK’s most powerful search tool to help you find people and ‘linked’ individuals.</p>
            </div>
        </div>
        <div class="row desktop-only">
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/director.png') ?>" />
                <p class="grid-head">Director Search</p>
                <p class="grid-content">It’s essential to know who you are dealing with and with Director Search, you can instantly check if someone is telling you the truth about their business dealing, if they’re disqualified or bankrupt, what other businesses they are involved with now and in the past. ORBIS helps you keep your own business healthy and free from bad-debts.</p>
            </div>
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/company.png') ?>" />
                <p class="grid-head">Company Search</p>
                <p class="grid-content">Finding information on PLC and Limited UK companies is relatively easy…but ORBIS goes much further providing detailed information on these AND partnerships, LLPs, sole-traders etc. We can show you a potential customer’s payment history with other businesses and give you a credit scoring indicator. Before you do business with someone, use ORBIS to get the full information on them.</p>
            </div>
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/tps.png') ?>" />
                <p class="grid-head">TPS/CTPS Checking</p>
                <p class="grid-content">Telephone Preference Scheme and Commercial Telephone Preference Scheme – if you do telemarketing, you need ORBIS. With a potential £5,000 fine for calling a TPS registered person or business, it’s just not worth the risk of not checking that number before you call. And we give you three different ways to check depending on your business practises.</p>
            </div>
        </div>
        <!-- googleoff: index -->
        <div class="orbisslider-mobile">
            <div>
                <img src="<?php echo base_url('img/grid/phone.png') ?>" />
                <p class="slide-head">Phone Search</p>
                <p class="slide-content">With 118 services costing as much as £20 per call, a low-cost alternative makes sense! Our phone number search gives you and your colleagues unlimited access to exactly the same directory data derived from all the UK’s telecom operators that drives the expensive voice services.</p>
            </div>
            <div>
                <img src="<?php echo base_url('img/grid/address.png') ?>" />
                <p class="slide-head">Address &amp; Postcode Lookup</p>
                <p class="slide-content">Royal Mail and Ordnance Survey between them have every addressable location in the UK listed. ORBIS gives you direct access via a simple search tool to this important resource. If your addressing needs are even greater, you can access this data via our powerful API. Just contact us for details.</p>
            </div>
            <div>
                <img src="<?php echo base_url('img/grid/person.png') ?>" />
                <p class="slide-head">Find a Person</p>
                <p class="slide-content">Our people finder is used by virtually every business and public sector type. Drawing on multiple resources and opted-in data sets, we offer what is probably the UK’s most powerful search tool to help you find people and ‘linked’ individuals.</p>
            </div>
            <div>
                <img src="<?php echo base_url('img/grid/director.png') ?>" />
                <p class="slide-head">Director Search</p>
                <p class="slide-content">It’s essential to know who you are dealing with and with Director Search, you can instantly check if someone is telling you the truth about their business dealing, if they’re disqualified or bankrupt, what other businesses they are involved with now and in the past. ORBIS helps you keep your own business healthy and free from bad-debts.</p>
            </div>
            <div>
                <img src="<?php echo base_url('img/grid/company.png') ?>" />
                <p class="slide-head">Company Search</p>
                <p class="slide-content">Finding information on PLC and Limited UK companies is relatively easy…but ORBIS goes much further providing detailed information on these AND partnerships, LLPs, sole-traders etc. We can show you a potential customer’s payment history with other businesses and give you a credit scoring indicator. Before you do business with someone, use ORBIS to get the full information on them.</p>
            </div>
            <div>
                <img src="<?php echo base_url('img/grid/tps.png') ?>" />
                <p class="slide-head">TPS/CTPS Checking</p>
                <p class="slide-content">Telephone Preference Scheme and Commercial Telephone Preference Scheme – if you do telemarketing, you need ORBIS. With a potential £5,000 fine for calling a TPS registered person or business, it’s just not worth the risk of not checking that number before you call. And we give you three different ways to check depending on your business practises.</p>
            </div>
        </div>
        <!-- googleon: index -->
        <div class="row">
            <div class="col-12 text-center">
                <p>
                    <p class="seppara text-center"><a class="btn btn-primary" href="<?php echo base_url('contact' ) ?>" role="button">Request a demo</a></p>
                </p>
            </div>
        </div>
    </div>
    <div class="sepbuffer withpara"></div>
</div>
<div id="sectors"></div>
<div id="simunix-slider">
    <p class="subtitle"><img src="<?php echo base_url("img/orbis.png") ?>" /></p>
    <h2>Sectors that use ORBIS</h2>
    <div class="gallery-slider">
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/orbis/po.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Police &amp; Security</p>
            <p class="slider-content">ORBIS provides information that aids investigations and ensures efficient use of man-power resources. Its integrated mapping, residential and business information and powerful search and analysis capabilities make it an essential tool for all law enforcement and security operations.</p>
            <p class="slider-content">
                <a class="btn btn-secondary" href="<?php echo base_url('orbis/police') ?>" role="button">Find out more</a>
            </p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/orbis/pr.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Property &amp; Lettings</p>
            <p class="slider-content">ORBIS helps companies within the property sales and lettings sector find the names of property owners and identify landlords of rental accommodation. The unique ZoneSearch feature allows you to obtain all the ownership and property details of houses within a specified area on a map, ideal for targeting certain areas with local marketing campaigns.</p>
            <p class="slider-content">
                <a class="btn btn-secondary" href="<?php echo base_url('orbis/estate-agents') ?>" role="button">Find out more</a>
            </p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/orbis/f.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Financial Services</p>
            <p class="slider-content">Look up a rental property address and find out the contact details of who owns it. This valuable information allows you to build a marketing database of landlords you can then approach to sell your lettings services.</p>
            <p class="slider-content">
                <a class="btn btn-secondary" href="<?php echo base_url('orbis/finance') ?>" role="button">Find out more</a>
            </p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/orbis/pe.jpg") ?>" width="328" height="328" />
            <p class="slider-title">People Tracing &amp; Debt Collection</p>
            <p class="slider-content">With ORBIS you can find a person from even just partial details, searching across multiple data sources of the UK electoral roll, companies house, phone book and consumer data. You can view their past and present properties, names of people living at the same addresses and any business co-directors.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/orbis/lo.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Logistics</p>
            <p class="slider-content">Used by the majority of high street delivery firms, ORBIS mapping visualises delivery addresses with a geo-search overlay facility giving the names of all property occupants. Powerful residential and company address search features ensure accurate delivery details for drivers even when the source address information is incomplete.</p>
            <p class="slider-content">
                <a class="btn btn-secondary" href="<?php echo base_url('orbis/logistics') ?>" role="button">Find out more</a>
            </p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/orbis/f.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Legal Services</p>
            <p class="slider-content">Many law firms throughout the UK use ORBIS for Land Registry information and details of household occupants, to assist with conveyancing and verifying property ownership. Person search and person verification within ORBIS are also vital tools for law firms trying to locate individuals in skip-tracing and debt collection work, plus the KYC and Anti-money laundering checks they are required to make.</p>
            <p class="slider-content">
                <a class="btn btn-secondary" href="<?php echo base_url('orbis/legal') ?>" role="button">Find out more</a>
            </p>
        </div>
    </div>
</div>
<div id="ourCustomers" class="container-fluid trustcontainer">
    <div class="sep"></div>
    <div class="row">
        <div class="col-12 text-center">
            <p class="leading">Trusted by 200+ companies and public sector organisations</p>
            <p class="content">ORBIS is essential to organisations of all sizes as these customers will testify…</p>
        </div>
    </div>
    <div class="row companyimages justify-content-center text-center">
        <div class="col-4 col-lg-2">
            <img class="bakertillycol" src="<?php echo base_url("img/companies/bakertillycol.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="hsbccol" src="<?php echo base_url("img/companies/hsbccol.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="amexcol" src="<?php echo base_url("img/companies/amexcol.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="yorkbuilcol" src="<?php echo base_url("img/companies/yorkbuilcol.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="tntcol" src="<?php echo base_url("img/companies/tntcol.png") ?>"/>
        </div>
    </div>
    <div class="sepbuffer"></div>
</div>
<div id="dataSources" class="container-fluid greycontainer">
    <div class="container">
        <div class="sep"></div>
        <div class="row">
            <div class="col-lg-5 sourcesinfograph text-right">
                <img class="infographic" src="<?php echo base_url('img/infographic.png')?>" />
            </div>
            <div class="offset-lg-2 col-lg-5 sourcescontent">
                <h2>How does ORBIS help your organisation?</h2>
                <p>ORBIS brings together people and business information from a variety of government, commercial and opted-in marketing sources and makes it available for searching, analysis, tele-appending or download to your desktop.</p>
                <div class="bluesquare"><img src="<?php echo base_url('img/bluesquare.png')?>" /></div>
                <p>The intuitive user interface requires no lengthy staff training or bulky manual. The clean, simple design belies the power ORBIS puts at your fingertips.</p>
                <div class="bluesquare"><img src="<?php echo base_url('img/bluesquare.png')?>" /></div>
                <p>Being web browser based, if your staff have Internet access, they already have ORBIS! Our server complex is designed to handle millions of simultaneous searches and deliver the results to your browser within seconds.</p>
                <p class="seppara"><a class="btn btn-primary" href="<?php echo base_url('contact' ) ?>" role="button">Contact us</a></p>
            </div>
            <div class="col-lg-6"></div>
        </div>
    </div>
</div>
<div id="pricing" class="container-fluid bluecontainer">
    <div class="sep"></div>
    <?php if(isset($_GET['m'])): ?>
        <p class="thankyou-message">Thank you for getting in touch. A member of our team will respond to your query as soon as possible</p>
    <?php else: ?>
    <div class="container">
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <h2>Packages that work for your business</h2>
            </div>
        </div>
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <p>Contact our Sales Experts to set up the right pricing for your business needs.</p>
            </div>
        </div>
        <form method="post" action="<?php echo base_url('orbis?m=thank_you'); ?>"  class="cpta_enabled_form">
		<input type="hidden" id="captcha_key" name="captcha_key"  />
            <div class="row">
                <div class="offset-lg-3 col-lg-3">
                    <div class="form-group">
                        <input name="first" type="text" class="form-control" placeholder="First name" required/>
                    </div>
					<div class="form-group">
                        <input type="text" name="last" class="form-control" placeholder="Last name" required/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="company" class="form-control" placeholder="Company" required/>
                    </div>
                    
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <input type="tel" name="phone" class="form-control" placeholder="Phone Number" required/>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" required/>
                    </div>
                    <div class="form-group">
                        <select name="enquiry" class="form-control">
                            <option value="0" selected disabled>Feature you're interested in</option>
                            <option value="orbis">ORBIS</option>
                            <option value="orbis-plus">ORBIS+</option>
                            <option value="orbis-network">ORBIS Network</option>
                        </select>
                    </div>
                    <div class="caret"><i class="far fa-angle-down"></i></div>
                </div>
            </div>
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <p><input type="submit" class="btn btn-primary" value="Send my request"/></p>
                </div>
            </div>
        </form>
    </div>
    <?php endif; ?>
</div>


 <div class="g-recaptcha" 
   data-sitekey="6LdDaQcaAAAAAF2WEbDYAKe7Q9Jucsqg_pjbyzrM"
   data-size="invisible"
   data-callback="formSubmit">
</div>

<script src="https://www.google.com/recaptcha/api.js"></script>