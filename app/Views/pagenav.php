<div class="pagenavcontainer">
    <ul class="pagenav">
    <?php foreach($elements as $element => $title): ?>
        <li><a href="#<?php echo $element ?>"<?php echo $element === array_key_first($elements) ? 'class="active"':''?>><?php echo $title ?></a></li>
    <?php endforeach; ?>
    </ul>
</div>

