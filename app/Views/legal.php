<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <p class="leading">Simple. Powerful. Reliable</p>
            <h1>ORBIS</h1>
            <p class="trailing">People and business searching at your fingertips. Used by businesses and organisations of all sizes, Orbis is the UK’s premier people and business search tool.</p>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<?php echo view('pagenav', $pageNav) ?>
<div id="searchType" class="container-fluid greycontainer">
    <div class="row">
        <div class="col-lg-7 text-right">
            <video class="landing-video" controls>

                <source src="<?php echo base_url('media/legal.mp4') ?>"
                        type="video/mp4">

                Sorry, your browser doesn't support embedded videos.
            </video>
            <img class="search-type" src="" />
        </div>
        <div class="col-lg-3 content">
            <p class="headsup">How it works</p>
            <h2>One Powerful System</h2>
            <p>ORBIS brings together many different UK people and business data sources for you to search in one easy-to-use powerful system.</p>
            <p>Search for UK individuals when dealing with debt cases or probate, property address searching and Land Registry information for conveyancing, and full access to Companies House data in commercial law cases.</p>
            <p>Being browser based, if your staff have internet access then they can also have ORBIS.</p>
            <p>Contact us to find out more about what ORBIS can do for your business</p>
            <div class="row contact">
                <div class="col-6">
                    <p><a class="btn btn-primary" href="<?php echo base_url('contact') ?>">Get in touch</a></p>
                </div>
                <div class="col-6">
                    <ul>
                        <li>+44 (0)1904 217765</li>
                        <li>orbis@simunix.com</li>
                    </ul>
                </div>
            </div>
            <p>ORBIS is also available as an API, allowing you to search our UK people and business data from within your own system. Please see our <a href="<?php echo base_url('t2a') ?>">T2A page</a> for more information.</p>
        </div>
        <div class="sepbuffer"></div>
    </div>
</div>
<div id="ourCustomers" class="container-fluid trustcontainer">
    <div class="sep"></div>
    <div class="row">
        <div class="col-12 text-center">
            <p class="leading">Trusted by 200+ companies and public sector organisations</p>
            <p class="content">ORBIS is essential to organisations of all sizes as these customers will testify…</p>
        </div>
    </div>
    <div class="row companyimages justify-content-center text-center">
        <div class="col-4 col-lg-2">
            <img class="bakertillycol" src="<?php echo base_url("img/companies/bakertillycol.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="hsbccol" src="<?php echo base_url("img/companies/hsbccol.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="amexcol" src="<?php echo base_url("img/companies/amexcol.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="yorkbuilcol" src="<?php echo base_url("img/companies/yorkbuilcol.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="klcol" src="<?php echo base_url("img/companies/klcol.png") ?>"/>
        </div>
    </div>
    <div class="sepbuffer"></div>
</div>
<div id="testimonials" class="container-fluid blackcontainer">
    <div class="sep"></div>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-3 text-right">
            <p class="headsup">Law Firms &amp; Solicitors</p>
            <p class="big-text">Trace individuals, verify identities</p>
        </div>
        <div class="col-12 col-lg-3 text-left">
            <p class="right-content">&ldquo;ORBIS has been an invaluable tool in
                finding people in the UK and verifying their
                identities in the course of our case investigations. With vast amounts of data connected up in one place it has saved hours of searching disparate information
                sources by our staff.&rdquo;</p>
            <p class="seppara"><a class="btn btn-primary" href="<?php echo base_url('contact' ) ?>" role="button">Contact Us</a></p>
        </div>
    </div>
</div>
<div id="useCases"></div>
<div id="simunix-slider">
    <p class="subtitle">What can we do?</p>
    <h2>How our customers are using ORBIS</h2>
    <div class="gallery-slider">
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/legal/pe.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Person Searching &amp; Identity Verification</p>
            <p class="slider-content">ORBIS combines many different UK data sources to allow you to trace a person, whether that be for liability of outstanding debts, finding the whereabouts of will beneficiaries mentioned in Probate, or finding someone involved in a criminal case.</p>
            <p class="slider-content">You can search for current or previous addresses and co-occupants are also shown. Perform your necessary KYC and Anti-money laundering checks on the Person Verification section within ORBIS to ensure your own, or your client’s, customers are genuinely who they say they are.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/legal/pr.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Property Searches &amp; Home Owner Verification</p>
            <p class="slider-content">For property searching and conveyancing, ORBIS has full access to HM Land Registry information, allowing you to identify the legal owner of a property, obtain LR title and plans.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/legal/f.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Searching UK Company Information</p>
            <p class="slider-content">Search Companies House data for information on UK companies including filed accounts and profit and loss reports. Use ORBIS to find company directors with links to other data sources for complete director profiles including property ownership, plus current and previous addresses detailing co-habitants.</p>
        </div>
    </div>
</div>
<div id="pricing" class="container-fluid bluecontainer">
    <div class="sep"></div>
    <?php if(isset($_GET['m'])): ?>
        <p class="thankyou-message">Thank you for getting in touch. A member of our team will respond to your query as soon as possible</p>
    <?php else: ?>
    <div class="container">
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <h2>Packages that work for your business</h2>
            </div>
        </div>
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <p>Contact our Sales Experts to set up the right pricing for your business needs.</p>
            </div>
        </div>
        <form method="post" action="<?php echo base_url('orbis?m=thank_you'); ?>"  class="cpta_enabled_form">
		<input type="hidden" id="captcha_key" name="captcha_key"  />
            <div class="row">
                <div class="offset-lg-3 col-lg-3">
                    <div class="form-group">
                        <input name="first" type="text" class="form-control" placeholder="First name" required/>
                    </div>
					<div class="form-group">
                        <input type="text" name="last" class="form-control" placeholder="Last name" required/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="company" class="form-control" placeholder="Company" required/>
                    </div>
                    
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <input type="tel" name="phone" class="form-control" placeholder="Phone Number" required/>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" required/>
                    </div>
                    <div class="form-group">
                        <select name="enquiry" class="form-control">
                            <option value="0" selected disabled>Feature you're interested in</option>
                            <option value="orbis">ORBIS</option>
                            <option value="orbis-plus">ORBIS+</option>
                            <option value="orbis-network">ORBIS Network</option>
                        </select>
                    </div>
                    <div class="caret"><i class="far fa-angle-down"></i></div>
                </div>
            </div>
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <p><input type="submit" class="btn btn-primary" value="Send my request"/></p>
                </div>
            </div>
        </form>
    </div>
    <?php endif; ?>
</div>


 <div class="g-recaptcha" 
   data-sitekey="6LdDaQcaAAAAAF2WEbDYAKe7Q9Jucsqg_pjbyzrM"
   data-size="invisible"
   data-callback="formSubmit">
</div>

<script src="https://www.google.com/recaptcha/api.js"></script>