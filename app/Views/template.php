<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="290">
    <meta property="og:image:height" content="290">
    <meta property="og:image" content="<?php echo base_url('img/oglogo.png') ?>">
    <title><?php echo $metatitle ?></title>
    <meta name="description" content="<?php echo $metadesc ?>">
    <meta name="author" content="Simunix">
    <meta id="viewport" name='viewport'>
	<meta name="msvalidate.01" content="0F9B3CD91F01F16012579CC41A5F9275" />
	
	<?php if (isset($canonical_url)): ?>
	<link rel="canonical" href="<?php echo $canonical_url; ?>" />
	<?php endif; ?>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url("favicon.ico"); ?>" />
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/platform/1.3.5/platform.min.js"></script>
    <script>
        (function(doc) {
            var viewport = document.getElementById('viewport');
            //console.log('>34>'+navigator.userAgent + '>>' + navigator.maxTouchPoints + '>>' + platform.version);
            if(typeof navigator.maxTouchPoints !== 'undefined') {
                if ( navigator.userAgent.match(/Mac OS X/i) && !navigator.userAgent.match(/iPhone/i) &&  navigator.maxTouchPoints > 0 && (platform.version <= 13 || platform.version == '13.1.1')) {
                    viewport.setAttribute("content", "initial-scale=1.5");
					//console.log('>>viewport 1.5');
                }
            }
        }(document));
    </script>
	
	<?php if(config('Simunix')->cfg_data['shop_api_mode'] == "P"): //we only do analytics on live enviroment ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-GYDV3Q4PL0"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('consent', 'default', {
		'ad_storage': 'denied',
		'ad_user_data': 'denied',
		'ad_personalization': 'denied',
		'analytics_storage': 'denied'
	  });

	  gtag('config', 'G-GYDV3Q4PL0');
	</script>
	<?php endif; ?>
    
    <link rel="stylesheet" href="https://use.typekit.net/tne6hys.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("css/bootstrap.min.css")."?v=".$version; ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("css/fontawesome/css/all.css")."?v=".$version; ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("js/slick/slick.css")."?v=".$version; ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("js/slick/slick-theme.css")."?v=".$version; ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("css/styles.css")."?v=".$version; ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("js/glitchdrop/glitchdrop.css")."?v=".$version; ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("js/squareslider/squareslider.css")."?v=".$version; ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("js/gallery/gallery.css")."?v=".$version; ?>"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100..900&display=swap" rel="stylesheet">
    <?php
    if($css_files) {
        echo css_paths($css_files);
    }
    ?>
	<script type="text/javascript" src="<?php echo base_url('js/jquery-latest.min.js') ?>"></script>

</head>

<body class="<?php echo $page ?>">

<?php echo view("cookie-control"); ?>
<script type="text/javascript">


  function formSubmit(response) {
	  // submit the form which now includes a g-recaptcha-response input
	   $.ajax({
				url: ajaxUrl + '/reCaptcha?callback=?',
				dataType: 'json',
				timeout: 15000,
				data: {
					'response': response
				},
				success: function (result) {       
					
					
					if (!result.status)
					{					
						grecaptcha.reset();
					}else{
						
						captcha_ok = true;
						$('#captcha_key').val(result.id);
						
						//probably submit form here
						$('.cpta_enabled_form').trigger('submit', [true]);
						
					}
				},
				error: function() {
					alert('validate recaptcha error');
					grecaptcha.reset();
				}
				
			});
	}

	$('document').ready(function(){
				
		$('.cpta_enabled_form').on('submit', function (e, skipRecaptcha) {
			if(skipRecaptcha) {
				
				return;
			}
			e.preventDefault();			
		    grecaptcha.reset();
			grecaptcha.execute();
		});
	  
	});
  


</script>
<?php echo $content ?>



</div>
<footer class="container-fluid">
    <div class="footer-container">
        <div class="row justify-content-center">
            <img src="<?php echo base_url('img/logoblue.png') ?>" id="simunixLogo" />
        </div>
        <div class="row justify-content-center">
            <div class="col-4 offset-lg-1 col-lg-2">
                <h3>Resources</h3>
                <ul class="list-unstyled">
                    <!--<li><a class="text-muted" href="#">Blog</a></li>-->
                    <li><a class="text-muted" href="<?php echo base_url('faq' ) ?>">FAQ</a></li>
                </ul>
            </div>
            <div class="offset-1 col-4 offset-lg-0 col-lg-2">
                <h3>Company</h3>
                <ul class="list-unstyled">
                    <li><a class="text-muted" href="<?php echo base_url("about") ?>">About Us</a></li>
                    <li><a class="text-muted" href="<?php echo base_url("about#careers") ?>">Careers</a></li>
                    <li><a class="text-muted" href="<?php echo base_url("contact") ?>">Contact</a></li>
					<li><a class="text-muted" href="<?php echo base_url("Simunix-ISMS-Executive-Summary-v1.1.pdf") ?>" target="_blank">Simunix ISMS Executive Summary</a></li>
                </ul>
            </div>
            <div class="col-4 offset-lg-0 col-lg-2">
                <h3>Products</h3>
                <ul class="list-unstyled">
                    <li><a class="text-muted" href="<?php echo base_url("orbis") ?>">ORBIS</a></li>
                    <li><a class="text-muted" href="<?php echo base_url("ukpb") ?>">ukphonebook.com</a></li>
                    <li><a class="text-muted" href="<?php echo base_url("t2a") ?>">T2A</a></li>
                    <li><a class="text-muted" href="<?php echo base_url("age-verify-uk") ?>">Age Verify UK</a></li>
                </ul>
            </div>
            <div class="offset-1 col-4 offset-lg-0 col-lg-2">
                <h3>Social</h3>
                <ul class="list-unstyled">
                    <!--<li><a class="text-muted" href="#">Facebook</a></li>-->
                    <li><a class="text-muted" href="https://twitter.com/Simunix_Ltd" target="_blank">X</a></li>
                    <li><a class="text-muted" href="https://www.linkedin.com/company/simunix-limited/" target="_blank">LinkedIn</a></li>
                </ul>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-12 col-lg-8">
                <hr>
            </div>
        </div>
        <div class="row justify-content-center footer-bottom">
            <!--<div class="col-12 order-2 order-lg-1 col-lg-6"><p class="bottom">Copyright Simunix&trade; <?php /*echo date('Y'); */?>. All rights reserved.</p></div>-->
            <div class="col-12 footer-center">
                <a href="https://certcheck.ukas.com/certification/a9f8019e-d4b8-5ed4-aeaa-4be41e3ec301" target="_blank"><img src="<?php echo base_url('img/certs/iso-iec.png') ?>" class="cert-img" /></a>
                <img src="<?php echo base_url('img/certs/ccs.png') ?>" class="cert-img" />
                <a href="<?php echo base_url('media/certs/pci.pdf') ?>" target="_blank"><img src="<?php echo base_url('img/certs/pci.png') ?>" class="cert-img" /></a>
                <img src="<?php echo base_url('img/certs/avpa.png') ?>" class="cert-img" />
                <a href="<?php echo base_url('media/certs/cep.pdf') ?>" target="_blank"><img src="<?php echo base_url('img/certs/cep.png') ?>" class="cert-img" /></a>
                <p class="bottom">Copyright Simunix&trade; <?php echo date('Y'); ?>. All rights reserved. | <a href="<?php echo base_url('terms' ) ?>">Terms &amp; Conditions</a> | <a href="<?php echo base_url('privacy' ) ?>">Privacy Policy</a></p>
            </div>
        </div>
        <div class="row justify-content-center">
    <!--	<div class="col-12 col-lg-8 ce_wrapper">
            <div class="col-12 order-1 order-lg-2 col-lg-2 text-lg-right"><p class="bottom"><a href="<?php /*echo base_url('terms' ) */?>">Terms &amp; Conditions</a> | <a href="<?php /*echo base_url('privacy' ) */?>">Privacy Policy</a> | <a href="<?php /*echo base_url('sitemap' ) */?>">Site Map</a></p></div>
            <div class="ce_msg"><p>Our IT systems have been tested to ensure they meet the high standards needed to protect us against cyber attacks. This website is secured using <strong>256-bit encryption</strong>.</p></div>
            <br class="clear" />
        </div>-->
        </div>

        <?php /*gew sent this 6/11/2023 */ ?>
        <script type="text/javascript">
        _linkedin_partner_id = "5467178";
        window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
        window._linkedin_data_partner_ids.push(_linkedin_partner_id);
        </script><script type="text/javascript">
        (function(l) {
        if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])};
        window.lintrk.q=[]}
        var s = document.getElementsByTagName("script")[0];
        var b = document.createElement("script");
        b.type = "text/javascript";b.async = true;
        b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
        s.parentNode.insertBefore(b, s);})(window.lintrk);
        </script>
        <noscript>
        <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=5467178&fmt=gif" />
        </noscript>
    </div>
</footer>
<script type="text/javascript">
		var ajaxUrl = '<?php echo base_url('ajax'); ?>';
        var baseUrl = '<?php echo base_url() ?>';
</script>
		
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
        integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
        crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo base_url("js/slick/slick.min.js")."?v=".$version; ?>"></script>
<script type="text/javascript" src="<?php echo base_url("js/bootstrap.min.js")."?v=".$version; ?>"></script>
<script type="text/javascript" src="<?php echo base_url("js/glitchdrop/glitchdrop.js")."?v=".$version; ?>"></script>
<script type="text/javascript" src="<?php echo base_url("js/squareslider/squareslider.js")."?v=".$version; ?>"></script>
<script type="text/javascript" src="<?php echo base_url("js/gallery/gallery.js")."?v=".$version; ?>"></script>
<script type="text/javascript" src="<?php echo base_url("js/typewriter.js")."?v=".$version; ?>"></script>
<script type="text/javascript" src="<?php echo base_url("js/thankyou.js")."?v=".$version; ?>"></script>
<script type="text/javascript" src="<?php echo base_url("js/scripts.js")."?v=".$version; ?>"></script>
<?php
    if($js_files) {
        echo js_paths($js_files);
    }
?>

</body>
</html>