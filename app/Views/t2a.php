<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <p class="leading">Person &amp; Location Data API</p>
            <h1>T2A</h1>
            <p class="trailing">T2A supplies software developers with the high quality data required for building professional websites and business applications. </p>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<?php
echo view('pagenav', $pageNav) ?>
<div id="keyFeatures" class="container-fluid whitecontainer">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 text-center">
            <h2>Find out what you can do with our API</h2>
            <div class="t2aslider">
                <div>
                    <img src="img/slider/address.png" />
                    <p class="slide-head">See who lives at an address</p>
                    <p class="slide-content">Obtain the names and telephone numbers of all residents who have lived, or are currently living, at a specified address.</p>
                </div>
                <div>
                    <img src="img/slider/age.png" />
                    <p class="slide-head">Age verification</p>
                    <p class="slide-content">Validate that someone is aged 18+ if you are selling age restricted products or services.</p>
                </div>
                <div>
                    <img src="img/slider/telephone.png" />
                    <p class="slide-head">Find a business telephone number</p>
                    <p class="slide-content">Search current UK telephone data to find UK business telephone numbers by name and/or location.</p>
                </div>
                <div>
                    <img src="img/slider/geo.png" />
                    <p class="slide-head">Check a telephone number against the TPS</p>
                    <p class="slide-content">Incorporate Telephone Preference Service checking of customer phone numbers into your website or CRM.</p>
                </div>
                <div>
                    <img src="img/slider/address.png" />
                    <p class="slide-head">Find a person</p>
                    <p class="slide-content">Search for a UK individual across our vast dataset of edited electoral roll, phone book and consumer data.</p>
                </div>
                <div>
                    <img src="img/slider/age.png" />
                    <p class="slide-head">Find a company</p>
                    <p class="slide-content">Search for a company and integrate Companies House information into your system such as credit reports and filed accounts.</p>
                </div>
            </div>
            <!-- googleoff: index -->
            <div class="t2aslider-mobile">
                <div>
                    <img src="img/slider/address.png" />
                    <p class="slide-head">See who lives at an address</p>
                    <p class="slide-content">Obtain the names and telephone numbers of all residents who have lived, or are currently living, at a specified address.</p>
                </div>
                <div>
                    <img src="img/slider/age.png" />
                    <p class="slide-head">Age verification</p>
                    <p class="slide-content">Validate that someone is aged 18+ if you are selling age restricted products or services.</p>
                </div>
                <div>
                    <img src="img/slider/telephone.png" />
                    <p class="slide-head">Find a business telephone number</p>
                    <p class="slide-content">Search current UK telephone data to find UK business telephone numbers by name and/or location.</p>
                </div>
                <div>
                    <img src="img/slider/geo.png" />
                    <p class="slide-head">Check a telephone number against the TPS</p>
                    <p class="slide-content">Incorporate Telephone Preference Service checking of customer phone numbers into your website or CRM.</p>
                </div>
                <div>
                    <img src="img/slider/address.png" />
                    <p class="slide-head">Find a person</p>
                    <p class="slide-content">Search for a UK individual across our vast dataset of edited electoral roll, phone book and consumer data.</p>
                </div>
                <div>
                    <img src="img/slider/age.png" />
                    <p class="slide-head">Find a company</p>
                    <p class="slide-content">Search for a company and integrate Companies House information into your system such as credit reports and filed accounts.</p>
                </div>
            </div>
            <!-- googleon: index -->
            <p class="seppara text-center"><a class="btn btn-primary" href="https://t2a.io" target="_blank" role="button">Explore T2A</a></p>
        </div>
    </div>
    <div class="sepbuffer withpara"></div>
</div>
<div id="documentation" class="container-fluid blackcontainer">
    <div class="sep"></div>
    <div class="row">
        <div class="col-12 offset-lg-4 col-lg-4 text-center">
            <p><img class="t2a-logo" src="<?php echo base_url("img/t2a.png")?>" /></p>
            <h2>A Guide to using T2A API</h2>
            <p>T2A is an Application Programming Interface (API) that provides fast, reliable and secure online transactional access to our data sets and validation services</p>
            <p>Our <a href="https://t2a.io/docs/index/introduction" target="_blank">documentation guide</a> gives full details of the API, intended for software developers who wish to use one or more of the T2A methods. We provide a simple XML/JSON for easy integration and code samples in popular languages including PHP, classic ASP and Javascript.</p>
            <p><a class="btn btn-primary" href="https://t2a.io/docs/index/introduction" target="_blank">Find out more</a></p>
        </div>
    </div>
</div>
<div id="ourCustomers" class="container-fluid trustcontainer">
    <div class="sep"></div>
    <div class="row">
        <div class="col-12 text-center">
            <p class="leading">Trusted by 200+ companies and public sector organisations</p>
        </div>
    </div>
    <div class="row companyimages justify-content-center text-center">
        <div class="col-4 col-lg-2">
            <img class="bakertillycol" src="<?php echo base_url("img/companies/bakertillycol.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="hsbccol" src="<?php echo base_url("img/companies/hsbccol.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="amexcol" src="<?php echo base_url("img/companies/amexcol.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="yorkbuilcol" src="<?php echo base_url("img/companies/yorkbuilcol.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="tntcol" src="<?php echo base_url("img/companies/tntcol.png") ?>"/>
        </div>
    </div>
    <div class="sepbuffer"></div>
</div>
<div id="contactUs" class="container-fluid bluecontainer">
    <div class="sep"></div>
    <?php if(isset($_GET['m'])): ?>
        <p class="thankyou-message">Thank you for getting in touch. A member of our team will respond to your query as soon as possible</p>
    <?php else: ?>
    <div class="container">
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <h2>Got a question about T2A?</h2>
            </div>
        </div>
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <p>Contact our Experts for help and information about our T2A API.</p>
            </div>
        </div>
        <form method="post" action="<?php echo base_url('t2a?m=thank_you'); ?>"  class="cpta_enabled_form">
		<input type="hidden" id="captcha_key" name="captcha_key"  />
            <div class="row">
                <div class="offset-lg-3 col-lg-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="first" placeholder="First name" required/>
                    </div>
					<div class="form-group">
                        <input type="text" class="form-control" name="last" placeholder="Last name" required/>
                    </div>
                    
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <input type="tel" class="form-control" name="phone" placeholder="Phone Number" required/>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Email" required/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <div class="form-group">
                        <textarea name="enquiry" class="form-control" placeholder="How can we help you today?" rows="4" required></textarea>
                    </div>
                    <p><input type="submit" class="btn btn-primary" value="Send my request"/></p>
                </div>
            </div>
        </form>
    </div>
    <?php endif; ?>
</div>



 <div class="g-recaptcha" 
   data-sitekey="6LdDaQcaAAAAAF2WEbDYAKe7Q9Jucsqg_pjbyzrM"
   data-size="invisible"
   data-callback="formSubmit">
</div>

<script src="https://www.google.com/recaptcha/api.js"></script>