
<?php echo view('navigation') ?>

<div class="container-fluid blackcontainer">
    <div class="container">
        <h1>Get in touch with us</h1>
        <div class="row">
            <div class="col-12 col-lg-6">
                <ul>
                    <li>
                        <p>Where are we?</p>
                        <p class="list-sub">Simunix Ltd, Middleham House 2-3, St. Marys Court, YORK, YO24 1AH</p>
                    </li>
                    <li>
                        <p>Give us a call</p>
                        <p class="list-sub">+44 (0)1904 217765</p>
                    </li>
                    <li>
                        <p>General enquiries</p>
                        <p class="list-sub"><a href="mailto:info@simunix.com">info@simunix.com</a></p>
                    </li>
                    <li>
                        <p>Sales enquiries</p>
                        <p class="list-sub"><a href="mailto:info@simunix.com">info@simunix.com</a></p>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-lg-3">
                <?php if(isset($_GET['m'])): ?>
                    <p class="thankyou-message">Thank you for getting in touch. A member of our team will respond to your query as soon as possible</p>
                <?php else: ?>
                <form method="post" action="<?php echo base_url('contact?m=thank_you'); ?>" class="cpta_enabled_form">
				<input type="hidden" id="captcha_key" name="captcha_key"  />
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Name" required/>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="organisation" placeholder="Company/Organisation" required/>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Email address" required/>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control" name="phone" placeholder="Telephone number" required/>
                    </div>
                    <div class="form-group">
                        <select name="enquiry" class="form-control">
                            <option value="" disabled selected>Enquiry type</option>
                            <option value="Price quotation">Price Quotation</option>
                            <option value="Technical Help">Technical Help</option>
                            <option value="Service Information">Service Information</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
					 <div class="caret"><i class="far fa-angle-down"></i></div>
					
						
						
                    <div class="form-group">
						
                        <input type="submit" value="Send" class="form-control btn btn-primary" />
                    </div>
                   
                </form>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

 <div class="g-recaptcha" 
   data-sitekey="6LdDaQcaAAAAAF2WEbDYAKe7Q9Jucsqg_pjbyzrM"
   data-size="invisible"
   data-callback="formSubmit">
</div>

<script src="https://www.google.com/recaptcha/api.js"></script>

