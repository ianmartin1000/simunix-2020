<nav id="primaryNav" class="navbar fixed-top navbar-expand-lg">
    <a class="navbar-brand" href="<?php echo base_url() ?>"><img src="<?php echo base_url('img/logo.png') ?>"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <img src="<?php echo base_url('img/mobile-toggler.png') ?>" />
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('about' ) ?>">About</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Accreditations
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item dropdown-toggle" href="#" id="isoDropdown">
                        ISO 27001
                    </a>
                    <div class="inner-dropdown-menu isoDropdown">
                        <a class="dropdown-item" target="_blank" href="https://certcheck.ukas.com/certification/a9f8019e-d4b8-5ed4-aeaa-4be41e3ec301">Online</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" target="_blank" href="<?php echo base_url('media/certs/iso-iec.pdf') ?>">Certification</a>
                    </div>
<!--                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?php /*echo base_url('ukpb' ) */?>">Crown Commercial Service</a>-->
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" target="_blank" href="<?php echo base_url('media/certs/cep.pdf') ?>">Cyber Essentials PLUS</a>
                    <div class="dropdown-divider"></div>
                    <p class="dropdown-item">AVPA</p>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" target="_blank" href="<?php echo base_url('media/certs/pci.pdf') ?>">PCI DSS Validation</a>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto right-nav">
            <li class="nav-item brand-nav">
                <a class="nav-link brand-nav" href="<?php echo base_url('orbis' ) ?>">ORBIS</a>
            </li>
            <li class="nav-item brand-nav">
                <a class="nav-link" href="<?php echo base_url('ukpb' ) ?>">UKPhonebook.com</a>
            </li>
            <li class="nav-item brand-nav">
                <a class="nav-link" href="<?php echo base_url('t2a' ) ?>">T2A</a>
            </li>
            <li class="nav-item brand-nav">
                <a class="nav-link" href="<?php echo base_url('age-verify-uk' ) ?>">Age Verify UK</a>
            </li>
            <li class="nav-item contact">
                <a class="nav-link" href="<?php echo base_url('contact' ) ?>">Contact Us</a>
            </li>
        </ul>
    </div>
</nav>