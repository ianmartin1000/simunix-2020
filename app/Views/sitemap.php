<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <h1>Site Map</h1>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<div class="container firstcontainer">
    <div class="row">
        <div class="col">
            <ul>
                <li>
                    <a href="<?php echo base_url('') ?>">Home</a>                    
                </li>
                <li>
                    <a href="<?php echo base_url('orbis') ?>">ORBIS</a>
                </li>
				<li>
                    <a href="<?php echo base_url('ukpb' ) ?>">UKPhonebook.com</a>
                </li>
				<li>
                    <a href="<?php echo base_url('t2a' ) ?>">T2A</a>
                </li>
				<li>
                    <a href="<?php echo base_url('about' ) ?>">About</a>
                </li>
				<li>
                    <a href="<?php echo base_url('about#careers' ) ?>">Careers</a>
                </li>
				<li>
                    <a href="<?php echo base_url('contact' ) ?>">Contact</a>
                </li>
				<li>
                    <a href="<?php echo base_url('terms' ) ?>">Terms &amp; Conditions</a>
                </li>
				<li>
                    <a href="<?php echo base_url('privacy' ) ?>">Privacy Policy</a>
                </li>
            </ul>
        </div>
    </div>
</div>

