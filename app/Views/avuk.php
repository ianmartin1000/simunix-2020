<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <p class="leading">Age verification based on data</p>
            <h1>Age Verify UK</h1>
            <p class="trailing">Our service matches your customer’s details against 120 million UK person addresses to verify if they are aged 18 or over.</p>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<?php echo view('pagenav', $pageNav) ?>
<div id="keyFeatures" class="container-fluid greycontainer">
    <div class="row">
        <div class="col-lg-7 text-right">
            <video class="landing-video" controls>

                <source src="<?php echo base_url('media/avuk.mp4') ?>"
                        type="video/mp4">

                Sorry, your browser doesn't support embedded videos.
            </video>
        </div>
        <div class="col-lg-3 content">
            <p class="headsup">Age Verify UK</p>
            <h2>Instant Age Verification for your business</h2>
            <ul>
                <li>Verification using data matching, not a tick box</li>
                <li>Ensures maximum compliance with UK legislation on age-restricted products</li>
                <li>Matches customers against 120 million UK person records</li>
                <li>Cost effective – only pay for customers that are checked</li>
                <li>No contract or minimum commitment</li>
                <li>Secure online GDPR compliant service</li>
            </ul>
        </div>
        <div class="sepbuffer"></div>
    </div>
</div>
<div class="container-fluid avukcontainer">
<div id="solutions" class="container choosecontainer">
    <div class="sep"></div>
    <div class="row">
        <div class="col-12 text-center">
            <p class="leading">Choose from three easy solutions</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-4">
            <h3>API integration with your website</h3>
            <img src="<?php echo base_url('img/option1.png') ?>" />
            <p>Integrate our API into your website or application to have instant age checks performed on customers seamlessly as part of your online checkout or registration process.</p>
        </div>
        <div class="col-12 col-lg-4">
            <h3>Age Verify Platform</h3>
            <img src="<?php echo base_url('img/option2.png') ?>" />
            <p>Use our web-based platform to perform age verifications on your customers. Simply register for an account and check one at a time or upload a batch file of names and addresses.</p>
        </div>
        <div class="col-12 col-lg-4">
            <h3>Wordpress and WooCommerce Plugin</h3>
            <img src="<?php echo base_url('img/option3.png') ?>" />
            <p>If your website is built in Wordpress, and uses WooCommerce shopping, then you can install our <a href="https://wordpress.org/plugins/t2a-age-verify/">Age Verify Plugin</a> for free.</p>
        </div>
    </div>
    <div class="sepbuffer"></div>
</div>
</div>
<div id="ourCustomers" class="container-fluid trustcontainer">
    <div class="sep"></div>
    <div class="row">
        <div class="col-12 text-center">
            <p class="leading">Trusted by 200+ companies</p>
        </div>
    </div>
    <div class="row companyimages justify-content-center text-center">
        <div class="col-4 col-lg-2">
            <img class="dpdcol" src="<?php echo base_url("img/companies/avuk/dpd.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="fedexcol" src="<?php echo base_url("img/companies/avuk/fedex.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="avhsbccol" src="<?php echo base_url("img/companies/avuk/hsbc.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="springcol" src="<?php echo base_url("img/companies/avuk/s.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="kfjcol" src="<?php echo base_url("img/companies/avuk/kfj.png") ?>"/>
        </div>
    </div>
    <div class="sepbuffer"></div>
</div>
<div id="contact" class="container-fluid bluecontainer">
    <div class="sep"></div>
    <?php if(isset($_GET['m'])): ?>
        <p class="thankyou-message">Thank you for getting in touch. A member of our team will respond to your query as soon as possible</p>
    <?php else: ?>
    <div class="container">
        <div class="row justify-content-center">
            <p><a class="btn btn-primary top" href="https://ageverifyuk.com" target="_blank">Visit Age Verify UK</a> <a class="btn btn-primary top" href="https://ageverifyuk.com#demo" target="_blank">Free Age Verification Demo</a></p>
        </div>
        <form method="post" action="<?php echo base_url('age-verify-uk?m=thank_you'); ?>"  class="cpta_enabled_form">
		<input type="hidden" id="captcha_key" name="captcha_key"  />
            <div class="row">
                <div class="offset-lg-3 col-lg-3">
                    <div class="form-group">
                        <input name="first" type="text" class="form-control" placeholder="First name" required/>
                    </div>
					<div class="form-group">
                        <input type="text" name="last" class="form-control" placeholder="Last name" required/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="company" class="form-control" placeholder="Company" required/>
                    </div>

                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <input type="tel" name="phone" class="form-control" placeholder="Phone Number" required/>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" required/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <p><input type="submit" class="btn btn-primary" value="Send my request"/></p>
                </div>
            </div>
        </form>
    </div>
    <?php endif; ?>
</div>


 <div class="g-recaptcha" 
   data-sitekey="6LdDaQcaAAAAAF2WEbDYAKe7Q9Jucsqg_pjbyzrM"
   data-size="invisible"
   data-callback="formSubmit">
</div>

<script src="https://www.google.com/recaptcha/api.js"></script>