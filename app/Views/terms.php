<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <h1>Terms &amp; Conditions</h1>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<div class="container firstcontainer">
    <div class="row">
        <div class="col">
            <p>Simunix Limited</p>
            <p><strong>1. Definitions</strong></p>
            <p>In this Contract the following terms have the definitions shown next to them:</p>
            <p>“Simunix” means Simunix Limited of the Middleham House 2-3, St. Marys Court, YORK, YO24 1AH, registered in England No. 3694982. Tel. +44 (0)1904 217765</p>
            <p>“Contract” means these Conditions.</p>
            <p>“Customer” means a user of the Service.</p>
            <p>“Data” means the name, address, telephone number and or any information made available by a third party advertiser on the Service.</p>
            <p>“Service” means the on-line telephone directory and postal address finder service which includes searching by business type, business name and residential searching by name and accessed at the web address;&nbsp;<a href="http://www.ukphonebook.com/">www.ukphonebook.com</a></p>
            <p>“Business Use” means the use of the service in a commercial, public service or government context including, but not limited to registered charities. (A corporate version of the service is available. Please contact Simunix on +44 (0)1904 217765 for details)</p>
            <p><strong>2. Provision of the service</strong></p>
            <p>2.1 Simunix provides the Service to the Customer on these terms and conditions.</p>
            <p>2.2 Simunix will provide the Service with reasonable skill and care.</p>
            <p>2.3 The Customer accepts and acknowledges that it is technically impracticable to provide a fault free Service and the Customer accepts that Simunix does not undertake to do so.</p>
            <p>2.4 Simunix does not warrant that the Data is free from errors or omissions.</p>
            <p>2.5 Simunix may suspend the Service for operational reasons such as maintenance or because of an emergency. Before doing so Simunix will give the Customer as much notice as possible when the Service will be suspended.</p>
            <p>2.6 This Service is provided solely for the Customer’s own use and the Customer will not resell the Service (or any part or facility of it) to any third party.</p>
            <p><strong>3. Use of the service</strong></p>
            <p>3.1 The Service must not be used in any way that does not comply with the terms of any legislation or any licence applicable to the Customer or that is in any way unlawful</p>
            <p>3.2 The Customer must not:</p>
            <p>(a) Engage in any form of automated searching or attempt to bulk-download information from the Service. The information delivered to The Customer as the result of a Search is for The Customer’s use only and must not be transferred, stored or exported to any other computerised system for the purpose of creating a secondary database other than a personal address book containing no more than 500 records.</p>
            <p>(b) provide, disclose or otherwise make available to any third party any portion of the Data and will use the Service and the Data only for its own personal, private use;or</p>
            <p>(c) use the Service as the sole source for generating mailing or address lists for direct marketing or other marketing purposes</p>
            <p>(d) use the Service for Business Use</p>
            <p>3.3 The Customer acknowledges that there may be implications under the Data Protection Act 1998 (or any other applicable statutory or European Community data protection requirements) if individual telephone numbers are added to a database and permanently stored.</p>
            <p>3.4 The Customer must not use the Service or the Data provided for any purpose except in accordance with the terms and conditions set out in this Contract.</p>
            <p>3.5 For the avoidance of doubt, the provisions of this paragraph 3 are not intended to prevent the Customer recording any individual item of Data, or disclosing any individual item of Data, free of charge, to friends or relations for non-commercial purposes.</p>
            <p>3.6 The Customer will comply with all reasonable instructions which Simunix may give from time to time regarding the use of the Service.</p>
            <p>3.7 The Customer hereby indemnifies Simunix against any claims or legal proceedings which are brought or threatened against Simunix by a third party because the Service is used in breach of paragraph 3.3, or because the Service is faulty and cannot be used by that third party.</p>
            <p><strong>4. Intellectual property rights</strong></p>
            <p>4.1 Where software is provided to enable the Customer to use the Service, Simunix grants the Customer a non-exclusive, non-transferable licence to use the software for that purpose.</p>
            <p>4.2 The Customer will not, without Simunix’s prior written consent, copy or (except as permitted by law) decompile or modify the software, nor copy the manuals or documentation.</p>
            <p>4.3 The Customer will sign any agreement reasonably required by the owner of the copyright in the software to protect the owner’s interest in that software.</p>
            <p>4.4 The Customer is not entitled to use any registered or unregistered trade mark, service mark or logo belonging to Simunix, nor to imply or infer any association with, connection to, authority from or approval by Simunix.</p>
            <p>4.5 You will receive, are receiving or have received information which is derived from databases (or parts or extracts thereof) of which Royal Mail is the owner or creator, or otherwise authorised to use (the “Data”). Royal Mail owns or is licensed, all Intellectual Property Rights which subsist in and/or relate to that Data from time to time. You must not at any time copy, reproduce, publish, sell, let, extract, reutilise or otherwise part with possession or control of or relay or disseminate any part of this information or use it for any purpose other than your own private or internal use. You shall be entitled to submit a maximum of fifteen (15) enquiries to this Look Up Service per day.</p>
            <p>OBLIGATIONS OF THE CUSTOMER (End-User) TO BT Wholesale Directory Solutions: (Guidline 8)</p>
            <p>The End-User shall:</p>
            <p>In all its dealings with or relating to information derived from BT’s OSIS database (“Information”) comply with all applicable laws and codes of practice including the Data Protection Act and the Code of Practice on Telecommunications Directory Information Covering the Fair Processing of Personal Data (21 December 1998);</p>
            <p>Use all reasonable endeavours in relation to the security and confidentiality of the Information in its custody or control to prevent any unauthorised disclosure of any part of it;</p>
            <p>If any complaint is made which relates to the End-user’s use of the Information then the End-user shall assist BT and the Licensee in investigating the complaint and shall take such steps as are reasonably necessary to remedy the complaint as soon as practicable;</p>
            <p>Only use or process any of the Information for their own internal purposes or, in the alternative, for a single use for a single specific person who is the End-user’s customer.</p>
            <p>The End-user shall not:</p>
            <p>Distribute, publish or display any material amount of the information by any means, except so that a single specific person can use it as permitted above.</p>
            <p>Export or permit the export of any material amount of the Information to a country which is not within the European Economic Area without the express consent of BT.</p>
            <p>MARKS: The End-user shall not have any rights to use the BT Marks and shall not make reference to BT or any BT product or service in any promotional or marketing advertising, communications, literature, or packaging.</p>
            <p>The End-user shall not alter any copyright or other intellectual property right acknowledgement or confidentiality marking incorporated into or applied to BT’s OSIS data or documentation owned by BT.</p>
            <p>The Customer shall be responsible for providing and maintaining:</p>
            <p>Appropriate Internet connection to access the Service;<br>
                Appropriate Internet browser software to access the Service;<br>
                Appropriate access controls to ensure the Use of the Service is in accordance with these Terms and Conditions.</p>
            <p><strong>5. Intellectual property rights indemnities</strong></p>
            <p>5.1 Simunix will indemnify the Customer against all claims and proceedings arising from infringement of any intellectual property rights by reason of Simunix’s provision of the Service to the Customer. As a condition of this indemnity the Customer must:</p>
            <p>(a) notify Simunix promptly in writing of any allegation of infringement;<br>
                (b) make no admission relating to the infringement;<br>
                (c) allow Simunix to conduct all negotiations and proceedings and give Simunix all reasonable assistance in doing so (Simunix will pay the Customer’s reasonable expenses for such assistance); and<br>
                (d) allow Simunix to modify the Service, or any item provided as part of the Service, so as to avoid the infringement, provided that the modification does not materially affect the performance of the Service.</p>
            <p>5.2 The indemnity in paragraph 5.1 does not apply to infringements caused by the use of the Service in conjunction with other equipment, software or services not supplied by Simunix or to infringements caused by designs or specifications made by, or on behalf of, the Customer. The Customer will indemnify Simunix against all claims, proceedings and expenses arising from such infringements.</p>
            <p>The limitations and exclusions of liability contained in paragraph 6 do not apply to this paragraph.</p>
            <p><strong>6. Limits of liability</strong></p>
            <p>6.1 Simunix accepts unlimited liability for death or personal injury resulting from its negligence.</p>
            <p>6.2 Simunix accepts no liability for any loss whatsoever caused by or attributed to any defect or failure of the Service for whatever reason, including without limitation, the inaccuracy of any portion of the Data.</p>
            <p><strong>7. Matters beyond the reasonable control of Simunix</strong></p>
            <p>If Simunix is unable to perform any obligation under this Contract because of a matter beyond its reasonable control, such as lightning, flood, exceptionally severe weather, fire, explosion, war, civil disorder, industrial disputes or acts of local or central Government or other competent authorities or events beyond the reasonable control of Simunix’s suppliers then Simunix will have no liability to the Customer for that failure to perform.</p>
            <p><strong>8. Termination of this contract or the service</strong></p>
            <p>Simunix may immediately terminate this Contract or suspend the Service either temporally or indefinitely at any time without any reference to the Customer.</p>
            <p><strong>9. Change to this contract</strong></p>
            <p>Simunix can change the Conditions of this Contract at any time. Simunix will give the Customer notice of the changes by email message.</p>
            <p><strong>10. Transferring this contract</strong></p>
            <p>The Customer cannot transfer or try to transfer this Contract, or any part of it, to anyone else.</p>
            <p><strong>11. Entire agreement</strong></p>
            <p>This Contract contains the whole agreement between the parties and supersedes all previous written or oral agreements relating to its subject matter.</p>
            <p><strong>12. Third party rights</strong></p>
            <p>The parties agree that the terms of this Contract are not enforceable by a third party under the Contracts (Rights of Third Parties) Act 1999.</p>
            <p><strong>13. Applicable law</strong></p>
            <p>This Contract is governed by the law of England and Wales and both parties submit to the non-exclusive jurisdiction of the English Courts.</p>
            <p><strong>14. Refunds and Expiry of Purchased Search Credits</strong></p>
            <p>Purchased search credits are non-refundable and unused credits will expire twelve (12) months after the date of purchase.</p>
            <p><strong>15. Free Search Credits</strong></p>
            <p>Search credits may, at the complete discretion of Simunix Ltd., be added to a User’s account free of charge on a daily, weekly or monthly basis. Free Search Credits will not normally be added to the accounts of Users from outside the United Kingdom unless a balance of Purchased Search Credits is maintained on the account.</p>
            <p><strong>16. Contacting By Email</strong></p>
            <p>Simunix Ltd may from time to time contact The Customer by email with news about our website, and offers from selected third-parties. By agreeing to these terms and conditions you are granting permission for us to contact you in this way.</p>
            <p><strong>17. Third-party Advertising</strong></p>
            <p>We allow third-party companies to serve ads and/or collect certain anonymous information when you visit our web site. These companies may use non-personally identifiable information (e.g., click stream information, browser type, time and date, subject of advertisements clicked or scrolled over) during your visits to this and other Web sites in order to provide advertisements about goods and services likely to be of greater interest to you. These companies typically use a cookie or third party web beacon to collect this information. To learn more about this behavioural advertising practice or to opt-out of this type of advertising, you can visit&nbsp;<a href="http://www.networkadvertising.org/" target="_blank" rel="noopener">www.networkadvertising.org</a></p>
            <p>You may manage your Online Behavioural Advertising (OBA) preferences by visiting…<br>
                <a href="http://www.networkadvertising.org/managing/opt_out.asp" target="_blank" rel="noopener">http://www.networkadvertising.org/managing/opt_out.asp</a><br>
                or<br>
                <a href="https://admin.valueclickmedia.com/optout/index.html" target="_blank" rel="noopener">https://admin.valueclickmedia.com/optout/index.html</a></p>
            <p><strong>18. Mobile Location Service</strong></p>
            <p>The user agrees to be located i.e. consents for the mobile operator to release their location data to the service provider for the provision of the service.</p>
            <p>That the service accuracy can vary depending on the user’s situation i.e. city vs. country.</p>
            <p>The service provider has the right to suspend or terminate the service.</p>
        </div>
    </div>
</div>
