<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <h1>FAQs</h1>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<div class="container firstcontainer">
    <div class="row">
        <div class="col">
            <ul>
                <li>
                    <p>What is ORBIS?</p>
                    <p class="list-sub">ORBIS brings together people and business information from a variety of government, commercial and opted-in marketing sources and makes it available for searching, analysis, tele-appending or download to your desktop.</p>
                </li>
                <li>
                    <p>What is T2A?</p>
                    <p class="list-sub">T2A offers all the same searching and data cleansing capabilities of ORBIS but is available as an API. Programmers and web developers can use T2A to integrate ORBIS functionality (like person searching and address lookups) into your website, application or CRM.</p>
                </li>
                <li>
                    <p>Is Simunix compliant with the General Data Protection Regulation (GDPR)?</p>
                    <p class="list-sub">Yes, please read our <a href="https://www.ukphonebook.com/gdpr">ukphonebook.com GDPR page</a> for further information.</p>
                </li>
                <li>
                    <p>Are you hiring?</p>
                    <p class="list-sub">We aren’t currently actively searching for new members to join the Simunix team but are always on the lookout. Please feel free to apply speculatively on our <a href="<?php echo base_url("about") ?>">careers section.<a></a></p>
                </li>
            </ul>
        </div>
    </div>
</div>
