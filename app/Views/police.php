<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <p class="leading">ORBIS for</p>
            <h1>Police &amp; Security services</h1>
            <p class="trailing">Used by the majority of the UK’s police forces, including the MET Police, ORBIS is a powerful tool enabling officers to search UK people and business information to aid investigations.</p>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<?php echo view('pagenav', $pageNav) ?>
<div id="power" class="container-fluid greycontainer">
    <div class="row">
        <div class="col-lg-7 text-right">
            <video class="landing-video" controls>

                <source src="<?php echo base_url('media/police.mp4') ?>"
                        type="video/mp4">

                Sorry, your browser doesn't support embedded videos.
            </video>
            <img class="search-type" src="" />
        </div>
        <div class="col-lg-3 content">
            <p class="headsup">How it works</p>
            <h2>Power of ORBIS</h2>
            <p>ORBIS brings together a vast amount of UK people and business data into one powerful search system. Being browser based, ORBIS is immediately available to all officers with internet access. The many uses of ORBIS for our Police customers include:</p>
            <ul>
                <li>Person Tracing using address searching where you can view co-occupants, aswell as an individuals links to previous addresses.</li>
                <li>Person Verification for identity checking.</li>
                <li>OSINT Investigations using the many data sources in ORBIS.</li>
                <li>Bulk Reverse Search mobile numbers to identify possible suspects.</li>
                <li>Intelligence Gathering in high crime areas using our ZoneSearch feature.</li>
                <li>Augmented Land Registry for locating landlords, investigating money laundering and human trafficking, etc.</li>
            </ul>
            <div class="row contact">
                <div class="col-12">
                    <p>
                        <a class="btn btn-primary" href="<?php echo base_url('contact') ?>">Get in touch</a>
                        
						<?php /*<a class="btn btn-secondary" href="https://www.applytosupply.digitalmarketplace.service.gov.uk/g-cloud/services/940523964439808">Details on G-Cloud</a>*/ ?>
                    </p>
                </div>
                <div class="col-6">
                    <ul>
                        <li>+44 (0)1904 217765</li>
                    </ul>
                </div>
                <div class="col-6">
                    <ul>
                        <li>orbis@simunix.com</li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="sepbuffer"></div>
    </div>
</div>
<div id="ourCustomers" class="container-fluid trustcontainer">
    <div class="sep"></div>
    <div class="row">
        <div class="col-12 text-center">
            <p class="leading">We are proud to supply ORBIS to the majority of the UK's Police Forces and Security Services.</p>
        </div>
    </div>
    <div class="row companyimages justify-content-center text-center">
        <div class="col-4 col-lg-2">
            <img class="gmp" src="<?php echo base_url("img/companies/police/gmp.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="wmp" src="<?php echo base_url("img/companies/police/wmp.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="met" src="<?php echo base_url("img/companies/police/met.png") ?>"/>
        </div>
        <div class="col-0 col-lg-2">
            <img class="ands" src="<?php echo base_url("img/companies/police/ands.png") ?>"/>
        </div>
        <div class="col-4 col-lg-2">
            <img class="ps" src="<?php echo base_url("img/companies/police/ps.png") ?>"/>
        </div>
    </div>
    <div class="sepbuffer"></div>
</div>
<div id="testimonials" class="container-fluid blackcontainer">
    <div class="sep"></div>
    <div class="row justify-content-center">
        <div class="col-12 col-lg-3 text-right">
            <p class="headsup">The Metropolitan Police Service</p>
            <p class="big-text">Using ORBIS to trace individuals &amp; verify identities</p>
        </div>
        <div class="col-12 col-lg-3 text-left">
            <p class="right-title">Detective Inspector</p>
            <p class="right-subtitle">Metropolitan Police</p>
            <p class="right-content">&ldquo;ORBIS is essential in identifying and verifying addresses and individuals that are important in our work. To see how long a person has lived at an address or to identify who lives where, with neighbour disputes, ORBIS is a key piece of software. The Bulk mobile reverse search feature further enhances our ability to trace and locate people, and drastically reduces police time spent on investigations.&rdquo;</p>
            <p class="seppara"><a class="btn btn-primary" href="<?php echo base_url('contact' ) ?>" role="button">Contact Us</a></p>
        </div>
    </div>
</div>
<div id="useCases"></div>
<div id="simunix-slider">
    <p class="subtitle">What can we do?</p>
    <h2>How our customers are using ORBIS</h2>
    <div class="gallery-slider">
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/police/pt.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Person Tracing</p>
            <p class="slider-content">From partial name and address details, or an email address or telephone number, officers can use our <strong>Person Search</strong> function to trace a person of interest. Our unique address linking capabilities allow you to view previous addresses, plus links to co-habitants at each property.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/police/idv.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Identity Verification</p>
            <p class="slider-content">ORBIS uses a number of data sources including the full Electoral Roll and BT Osis data. Police users are able to conduct identity verification for a variety of reasons, such as fraud checks and anti-money laundering, by matching the name and address details they have with over 48 million records in ORBIS.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/police/osint.jpg") ?>" width="328" height="328" />
            <p class="slider-title">OSINT Investigations</p>
            <p class="slider-content">Monitor social media posts online using the Maps/Geo-search function. Enabling geo-tags will show social media posts from the last 7 days within a specified radius on the map.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/police/bsrmn.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Bulk Reverse Search Mobile Numbers</p>
            <p class="slider-content">Our Police customers often use ORBIS to identify names and addresses of people who are in the contact list of a suspect’s mobile phone. Officers can simply download a list of mobile numbers from a seized phone and use our <strong>Bulk Mobile Number Reverse Search</strong> to get matches to the users of those numbers and see if a line is in an active contract.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/police/ig.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Intelligence Gathering in high crime areas</p>
            <p class="slider-content">Police users of ORBIS can take advantage of the <strong>Zonesearch</strong> feature which allows them to draw a polygon on a map of an area of interest, such as a high crime area, and gather intelligence on the residents of the properties within it. This valuable information can then be passed on to PCSO’s when making house to house enquiries.</p>
        </div>
        <div class="slider-item">
            <img src="<?php echo base_url("img/gallery/police/lr.jpg") ?>" width="328" height="328" />
            <p class="slider-title">Augmented Land Registry Information</p>
            <p class="slider-content">ORBIS combines Land Registry with other data sources enabling officers to locate and identify landlords. This is useful for a range of different types of investigation including money laundering and human trafficking or modern slavery cases.</p>
        </div>
    </div>
</div>
<div id="pricing" class="container-fluid bluecontainer">
    <div class="sep"></div>
    <?php if(isset($_GET['m'])): ?>
        <p class="thankyou-message">Thank you for getting in touch. A member of our team will respond to your query as soon as possible</p>
    <?php else: ?>
    <div class="container">
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <h2>Packages that work for your force</h2>
            </div>
        </div>
        <div class="row">
            <div class="offset-lg-3 col-lg-6">
                <p>ORBIS has flexible pricing options, a free trial service available, and an unlimited number of users are permitted on one single licence. Full details of ORBIS, including security standards and certifications, can be found on the Government’s G-Cloud framework.</p>
            </div>
        </div>
        <form method="post" action="<?php echo base_url('orbis?m=thank_you'); ?>"  class="cpta_enabled_form">
		<input type="hidden" id="captcha_key" name="captcha_key"  />
            <div class="row">
                <div class="offset-lg-3 col-lg-3">
                    <div class="form-group">
                        <input name="first" type="text" class="form-control" placeholder="First name" required/>
                    </div>
					<div class="form-group">
                        <input type="text" name="last" class="form-control" placeholder="Last name" required/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="company" class="form-control" placeholder="Company" required/>
                    </div>
                    
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <input type="tel" name="phone" class="form-control" placeholder="Phone Number" required/>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" required/>
                    </div>
                    <div class="form-group">
                        <select name="enquiry" class="form-control">
                            <option value="0" selected disabled>Feature you're interested in</option>
                            <option value="orbis">ORBIS</option>
                            <option value="orbis-plus">ORBIS+</option>
                            <option value="orbis-network">ORBIS Network</option>
                        </select>
                    </div>
                    <div class="caret"><i class="far fa-angle-down"></i></div>
                </div>
            </div>
            <div class="row">
                <div class="offset-lg-3 col-lg-6">
                    <p><input type="submit" class="btn btn-primary" value="Send my request"/></p>
                </div>
            </div>
        </form>
    </div>
    <?php endif; ?>
</div>


 <div class="g-recaptcha" 
   data-sitekey="6LdDaQcaAAAAAF2WEbDYAKe7Q9Jucsqg_pjbyzrM"
   data-size="invisible"
   data-callback="formSubmit">
</div>

<script src="https://www.google.com/recaptcha/api.js"></script>