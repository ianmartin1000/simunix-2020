<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <p class="leading">More than just directory enquiries</p>
            <h1>UKPhonebook.com</h1>
            <p class="trailing">Our public facing ukphonebook.com website allows you to search for people and businesses throughout the UK, including names, addresses, age guides, property prices, company & director reports and much more.</p>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<?php echo view('pagenav', $pageNav) ?>
<div id="demonstration" class="container-fluid whitecontainer">
    <div class="row">
        <div class="col-lg-7 text-right">
            <img class="search-type" src="<?php echo base_url('img/tempgroup4.png') ?>" />
        </div>
        <div class="col-lg-3 content">
            <p class="headsup">Search Type</p>
            <h2>Search People</h2>
            <p>Find UK residential and business phone numbers listed by name with our easy to use online telephone directory service. </p>
            <p><a class="btn btn-primary" href="https://www.ukphonebook.com">Visit ukphonebook.com</a></p>
        </div>
        <div class="sepbuffer"></div>
    </div>
</div>
<div id="dataSources" class="container-fluid blackcontainer">
    <div class="sep"></div>
    <div class="row">
        <div class="offset-lg-2 col-lg-3">
            <h2>The most powerful and current data</h2>
            <p>We bring together over 130 million records on people and businesses throughout the UK and make it available to you for searching and analysis on Ukphonebook.com.
                Our database links up data from a variety of government, commercial and opted-in marketing sources using only the most up-to-date information available.
            </p>
            <p class="seppara text-center"><a class="btn btn-primary" href="#" role="button">Try it now</a></p>
        </div>
        <div class="offset-lg-2 col-lg-3 text-lg-center">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <img src="<?php echo base_url('img/gridblack/address.png') ?>" />
                    <p class="gridblack-head"><a href="https://www.ukphonebook.com/postcode-finder">Postal Address Finder</a></p>
                    <p class="gridblack-content">We hold every addressable location in the UK by combining both Royal Mail and Ordnance Survey data.</p>
                </div>
                <div class="col-12 col-lg-6">
                    <img src="<?php echo base_url('img/gridblack/er.png') ?>" />
                    <p class="gridblack-head"><a href="https://www.ukphonebook.com/er-search">UK Electoral Roll</a></p>
                    <p class="gridblack-content">The definitive Government source of information on every household within the UK.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-6">
                    <img src="<?php echo base_url('img/gridblack/companies.png') ?>" />
                    <p class="gridblack-head"><a href="https://www.ukphonebook.com/company-information">Companies House</a></p>
                    <p class="gridblack-content">Information on companies and directors for every limited company based, or with an office, in the UK.</p>
                </div>
                <div class="col-12 col-lg-6">
                    <img src="<?php echo base_url('img/gridblack/phone.png') ?>" />
                    <p class="gridblack-head"><a href="https://www.ukphonebook.com/">UK Telephone Directory</a></p>
                    <p class="gridblack-content">Access to all numbers in the phone book for much less than calls to 118 directory enquiries services.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="sepbuffer"></div>
</div>
<div id="keyFeatures" class="container-fluid whitecontainer">
    <div class="sep"></div>
    <h2 class="text-center">What can you do on ukphonebook.com?</h2>
    <div class="container">
        <div class="row desktop-only">
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/phone.png') ?>" />
                <p class="grid-head">Phone Search</p>
                <p class="grid-content">With 118 services costing as much as £20 per call, a low-cost alternative makes sense! Our phone number search gives you and your colleagues unlimited access to exactly the same directory data derived from all the UK’s telecom operators that drives the expensive voice services.</p>
            </div>
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/address.png') ?>" />
                <p class="grid-head">Address &amp; Postcode Lookup</p>
                <p class="grid-content">Royal Mail and Ordnance Survey between them have every addressable location in the UK listed. ORBIS gives you direct access via a simple search tool to this important resource. If your addressing needs are even greater, you can access this data via our powerful API. Just contact us for details.</p>
            </div>
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/person.png') ?>" />
                <p class="grid-head">Find a Person</p>
                <p class="grid-content">Our people finder is used by virtually every business and public sector type. Drawing on multiple resources and opted-in data sets, we offer what is probably the UK’s most powerful search tool to help you find people and ‘linked’ individuals.</p>
            </div>
        </div>
        <div class="row desktop-only">
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/director.png') ?>" />
                <p class="grid-head">Director Search</p>
                <p class="grid-content">It’s essential to know who you are dealing with and with Director Search, you can instantly check if someone is telling you the truth about their business dealing, if they’re disqualified or bankrupt, what other businesses they are involved with now and in the past. ORBIS helps you keep your own business healthy and free from bad-debts.</p>
            </div>
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/company.png') ?>" />
                <p class="grid-head">Company Search</p>
                <p class="grid-content">Finding information on PLC and Limited UK companies is relatively easy…but ORBIS goes much further providing detailed information on these AND partnerships, LLPs, sole-traders etc. We can show you a potential customer’s payment history with other businesses and give you a credit scoring indicator. Before you do business with someone, use ORBIS to get the full information on them.</p>
            </div>
            <div class="col-4">
                <img src="<?php echo base_url('img/grid/tps.png') ?>" />
                <p class="grid-head">TPS/CTPS Checking</p>
                <p class="grid-content">Telephone Preference Scheme and Commercial Telephone Preference Scheme – if you do telemarketing, you need ORBIS. With a potential £5,000 fine for calling a TPS registered person or business, it’s just not worth the risk of not checking that number before you call. And we give you three different ways to check depending on your business practises.</p>
            </div>
        </div>
        <!-- googleoff: index -->
        <div class="ukpbslider-mobile">
            <div>
                <img src="<?php echo base_url('img/grid/phone.png') ?>" />
                <p class="slide-head">Phone Search</p>
                <p class="slide-content">With 118 services costing as much as £20 per call, a low-cost alternative makes sense! Our phone number search gives you and your colleagues unlimited access to exactly the same directory data derived from all the UK’s telecom operators that drives the expensive voice services.</p>
            </div>
            <div>
                <img src="<?php echo base_url('img/grid/address.png') ?>" />
                <p class="slide-head">Address &amp; Postcode Lookup</p>
                <p class="slide-content">Royal Mail and Ordnance Survey between them have every addressable location in the UK listed. ORBIS gives you direct access via a simple search tool to this important resource. If your addressing needs are even greater, you can access this data via our powerful API. Just contact us for details.</p>
            </div>
            <div>
                <img src="<?php echo base_url('img/grid/person.png') ?>" />
                <p class="slide-head">Find a Person</p>
                <p class="slide-content">Our people finder is used by virtually every business and public sector type. Drawing on multiple resources and opted-in data sets, we offer what is probably the UK’s most powerful search tool to help you find people and ‘linked’ individuals.</p>
            </div>
            <div>
                <img src="<?php echo base_url('img/grid/director.png') ?>" />
                <p class="slide-head">Director Search</p>
                <p class="slide-content">It’s essential to know who you are dealing with and with Director Search, you can instantly check if someone is telling you the truth about their business dealing, if they’re disqualified or bankrupt, what other businesses they are involved with now and in the past. ORBIS helps you keep your own business healthy and free from bad-debts.</p>
            </div>
            <div>
                <img src="<?php echo base_url('img/grid/company.png') ?>" />
                <p class="slide-head">Company Search</p>
                <p class="slide-content">Finding information on PLC and Limited UK companies is relatively easy…but ORBIS goes much further providing detailed information on these AND partnerships, LLPs, sole-traders etc. We can show you a potential customer’s payment history with other businesses and give you a credit scoring indicator. Before you do business with someone, use ORBIS to get the full information on them.</p>
            </div>
            <div>
                <img src="<?php echo base_url('img/grid/tps.png') ?>" />
                <p class="slide-head">TPS/CTPS Checking</p>
                <p class="slide-content">Telephone Preference Scheme and Commercial Telephone Preference Scheme – if you do telemarketing, you need ORBIS. With a potential £5,000 fine for calling a TPS registered person or business, it’s just not worth the risk of not checking that number before you call. And we give you three different ways to check depending on your business practises.</p>
            </div>
        </div>
        <!-- googleon: index -->
    </div>
    <div class="sepbuffer withpara"></div>
</div>
<div id="visitUs" class="container-fluid bluecontainer">
    <div class="row justify-content-center">
        <p><a class="btn btn-secondary" href="https://ukphonebook.com" target="_blank">Visit ukphonebook.com</a></p>
    </div>
</div>
