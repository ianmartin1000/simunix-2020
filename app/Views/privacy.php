<div class="jumbotron jumbotron-fluid">
    <?php echo view('navigation') ?>
    <div class="jumbocontent">
        <div class="jumbocontentinner">
            <h1>Privacy Policy</h1>
        </div>
    </div>
    <div class="jumbosquare"></div>
</div>
<div class="container firstcontainer">
    <div class="row">
        <div class="col">
		<p><strong>Last update:</strong> 11 July 2024</p>
            <p><b>We, Our or Us means Simunix Ltd, Middleham House 2-3, St. Marys Court, YORK, YO24 1AH</b></p>
            <p>When you contact Us, We may need to collect certain information about you including your name, postal address, e-mail address, telephone number and other information that We may reasonably require. We gather this information to allow us to process any enquiries you make and it is then used by Us to provide you with a suitable solution for your needs.</p>
            <p>When you use Our website, We may also use aggregate information and statistics for the purposes of monitoring website usage in order to help us develop the website and our services. These statistics may include information that can be used to identify individuals.</p>
            <p>We may also wish to provide you with information by direct mail, email or telephone about some special features of Our products or any other service We think may be of interest to you. However, you are entitled to object to the processing of your personal information for direct marketing purposes and if you would rather not receive this information, please send an e-mail to <a href="mailto:admin@simunix.com">admin@simunix.com</a>. We do not pass your information to third parties.</p>
            <p>Cookies Policy</p>
            <p><b>About cookies</b></p>
            <p>Cookies are information packets sent by web servers to web browsers, and stored by the web browsers.</p>
            <p>The information is then sent back to the server each time the browser requests a page from the server. This enables a web server to identify and track web browsers.</p>
            <p>There are two main kinds of cookies: session cookies and persistent cookies. Session cookies are deleted from your computer when you close your browser, whereas persistent cookies remain stored on your computer until deleted, or until they reach their expiry date.</p>
			
			<p><strong>Cookies on our website</strong></h3>
					
					<p class="open_cookie_control"><a href="javascript:CookieControl.open();" class="btn btn-primary btn-lg">Update cookie settings</a></p>
			
				
				
					<br />
					
					<h3>Information about individual cookies</h3>
					<p>You can find more information about the individual cookies we use and the purposes for which we use them in the table below:</p>
					
					
					<div class="cookie-table">
						<table class="table privacy-table">
							<tr>
							<td><strong>Type</strong></td><td><strong>Cookie</strong></td><td><strong>Name</strong></td><td><strong>Purpose</strong></td><td><strong>When Expires</strong></td>
						</tr>
						
						<tr><td>Strictly Necessary</td><td>reCAPTCHA </td><td><em>_GRECAPTCHA</em></td><td>Sign in form and searches require reCAPTCHA. Google reCAPTCHA sets a necessary cookie (_GRECAPTCHA) when executed for the purpose of providing its risk analysis.</td><td>6 months</td></tr>
						<tr><td>Analytics</td><td>Google Analytics</td><td><em>_ga* </em></td><td>Website Analytics</td><td>1 year</td></tr>
						<tr><td>Analytics</td><td>Google Analytics</td><td><em>_ga	</em></td><td>ID used to identify users</td><td>1 year</td></tr>
						<tr><td>Analytics</td><td>Google Analytics</td><td><em>_gid	</em></td><td>ID used to identify users for 24 hours</td><td>24 hours</td></tr>
						
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>AnalyticsSyncHistory</em></td><td>Used to store information about the time a sync with the lms_analytics cookie took place for users in the Designated Countries</td><td>30 days</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>UserMatchHistory</em></td><td>These cookies are set by LinkedIn for advertising purposes, including: tracking visitors so that more relevant ads can be presented, allowing users to use the 'Apply with LinkedIn' or the 'Sign-in with LinkedIn' functions, collecting information about how visitors use the site, etc.</td><td>30 days</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>bcookie</em></td><td>Used by LinkedIn to track the use of embedded services.</td><td>1 year</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>bscookie</em></td><td>Used by LinkedIn to track the use of embedded services.</td><td>1 year</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>lang</em></td><td>Used to remember a user's language setting</td><td>session</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>li_gc</em></td><td>Used to store guest consent to the use of cookies for non-essential purposes</td><td>2 years</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>li_oatml</em></td><td>Collects information about how visitors use our site.</td><td>30 days</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>li_rm</em></td><td>Used as part of the LinkedIn Remember Me feature and is set when a user clicks Remember Me on the device to make it easier for him or her to sign in to that device</td><td>1 year</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>li_sugr</em></td><td>Used to make a probabilistic match of a user's identity outside the Designated Countries</td><td>3 months</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>liap</em></td><td>Cookie used for Sign-in with Linkedin and/or to allow for the Linkedin follow feature.</td><td>90 days</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>lidc</em></td><td>Used by the social networking service LinkedIn, for tracking the use of embedded services.</td><td>24 hours</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>lissc</em></td><td></td><td>1 year</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>ln_or</em></td><td>Used to determine if Oribi analytics can be carried out on a specific domain</td><td>1 day</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>spectroscopyId</em></td><td>These cookies are set by LinkedIn for advertising purposes, including: tracking visitors so that more relevant ads can be presented, allowing users to use the 'Apply with LinkedIn' or the 'Sign-in with LinkedIn' functions, collecting information about how visitors use the site, etc.</td><td>session</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>trkCode</em></td><td>This cookie is used by LinkedIn to support the functionality of adding a panel invite labeled 'Follow Us'</td><td>1 year</td></tr>
						<tr><td>Targeting</td><td>LinkedIn</td><td><em>trkInfo</em></td><td>This cookie is used by LinkedIn to support the functionality of adding a panel invite labeled 'Follow Us'</td><td>1 year</td></tr>
						
						
						</table>
					</div><br />
					
            <p><b>Google cookies</b></p>
            <p><a href="https://simunix.com/">simunix.com</a> uses Google Analytics to analyse the use of information about website use by means of cookies, which are stored on Users’ computers. The information generated relating to our website is used to create reports about the use of the website. Google will store and use this information. Google’s privacy policy is available at:&nbsp;<a href="http://www.google.com/privacypolicy.html">http://www.google.com/privacypolicy.html</a>.</p>
            <p><b>Refusing cookies</b></p>
            <p>Most browsers allow you to refuse to accept cookies.</p>
            <p>In Internet Explorer, you can refuse all cookies by clicking “Tools”, “Internet Options”, “Privacy”, and selecting “Block all cookies” using the sliding selector.</p>
            <p>In Firefox, you can adjust your cookies settings by clicking “Tools”, “Options” and “Privacy”.</p>
            <p>Blocking cookies will have a negative impact upon the usability of <a href="https://simunix.com/">simunix.com</a></p>
        </div>
    </div>
</div>
