<?php

    function js_paths(array $files){
        $jsPaths = '';
        foreach ($files as $file) {
            if(file_exists(getcwd() . '\js\\'.$file.'.js')) {
                $jsPaths .= "<script type=\"text/javascript\" src=\"" . base_url("js/" . $file . ".js") . "?v=".config('Simunix')->cfg_data['version']."\"></script>\n";
            }
        }
        return $jsPaths;
    }

    function css_paths(array $files){
        $cssPaths = '';
        foreach ($files as $file) {
            if(file_exists(getcwd() . '\css\\'.$file.'.css')) {
                $cssPaths .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . base_url("css/" . $file . ".css") . "?v=".config('Simunix')->cfg_data['version']."\"/>\n";
            }
        }
        return $cssPaths;
    }