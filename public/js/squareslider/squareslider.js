$(document).ready(function () {
    var s1 = $('.pri1'),
        s2 = $('.pri2'),
        s3 = $('.pri3'),
        g1 = $('.group1').children('span'),
        g2 = $('.group2').children('span');

    $(document).ready(function () {
        var s1    = $('.pri1'),
            s2    = $('.pri2'),
            s3    = $('.pri3'),
            g1    = $('.group1').children('span'),
            g2    = $('.group2').children('span'),
            start = 1,
            direction;

        //slideGroupRight(g1, s1, s2);
        //slideGroupLeft(g2, s3, s2);
        setTimeout(squareSlider(), 6000);

        function squareSlider() {
            var t;
            $('.imgGroup').hide();

            if(start == 1) {
                direction = "right"
                $('.imgGroup2').fadeIn();
                slideGroupRight(g1, s1, s2);
                start++;
            } else if(start == 3) {
                direction = "left";
                $('.imgGroup2').fadeIn();
                slideGroupLeft(g2, s3, s2);
                start--;
            } else {
                if(direction == "right") {
                    $('.imgGroup3').fadeIn();
                    slideGroupRight(g2, s2, s3);
                    start++;
                } else {
                    $('.imgGroup1').fadeIn();
                    slideGroupLeft(g1, s2, s1);
                    start--;
                }
            }


            t = setTimeout(squareSlider, 6000);
        }

        function slideGroupRight(g, s, e) {
            var tot = g.length - 1,
                cg = 0,
                t;

            slideIt();

            function slideIt() {
                if (cg > tot) {
                    $(e).animate({"color": "#0953FF"});
                    $(g[cg - 1]).animate({"color": "#DBDBDB"});
                    clearTimeout(t);
                } else {
                    if (cg > 0) {
                        $(g[cg - 1]).animate({"color": "#DBDBDB"});
                    } else {
                        $(s).animate({"color": "#DBDBDB"});
                    }
                    $(g[cg]).animate({"color": "#0953FF"});
                    cg++;
                    t = setTimeout(slideIt, 30);
                }
            }

        }

        function slideGroupLeft(g, s, e) {
            var tot = g.length - 1,
                cg = tot,
                t;

            slideIt();

            function slideIt() {
                if (cg < 0) {
                    $(e).animate({"color": "#0953FF"});
                    $(g[cg + 1]).animate({"color": "#DBDBDB"});
                    clearTimeout(t);
                } else {
                    if (cg < tot) {
                        $(g[cg + 1]).animate({"color": "#DBDBDB"});
                    } else {
                        $(s).animate({"color": "#DBDBDB"});
                    }
                    $(g[cg]).animate({"color": "#0953FF"});
                    cg--;
                    t = setTimeout(slideIt, 30);
                }
            }

        }
    });
});