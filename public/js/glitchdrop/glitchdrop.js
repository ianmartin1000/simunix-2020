$(document).ready(function () {
    pinAnimate();
});

function pinAnimate() {
    var pa = $('.pin').length;
    if(pa > 1) {
        $('.pin').eq(0).fadeIn();
        $('.pin').eq(0).animate({top: "50px"}, 200, "linear", function(){
            $('.pin-icon').eq(0).fadeIn();
            $('.pin-text').eq(0).children('.glitch').addClass('glitchOn');
            pinGlitch(0);

            $('.pin').eq(1).fadeIn();
            $('.pin').eq(1).animate({top: "50px"}, 200, "linear", function(){
                $('.pin-icon').eq(1).fadeIn();
                $('.pin-text').eq(1).children('.glitch').addClass('glitchOn');;
                pinGlitch(1);

                $('.pin').eq(2).fadeIn();
                $('.pin').eq(2).animate({top: "50px"}, 200, "linear", function(){
                    $('.pin-icon').eq(2).fadeIn();
                    $('.pin-text').eq(2).children('.glitch').addClass('glitchOn');
                    pinGlitch(2);
                });
            });
        });
    }
    else {
        $('.pin').fadeIn();
        $('.pin').animate({top: "50px"}, 100, "linear", function(){
            $('.pin-icon').fadeIn();
            $('.glitch').addClass('glitchOn');
            pinGlitch(0);
        });
    }

}

function pinGlitch(num) {
    $('.pin-text').eq(num).children("[data-text]").attr("data-text", function (i, txt) {
        var $typer = $(this),
            tot = txt.length,
            pauseMax = 60,
            pauseMin = 60,
            ch = 0,
            t;

        typeIt();

        function typeIt() {
            if (ch > tot) {
                clearTimeout(t);
                glitchIt();
            } else {
                $typer.text(txt.substring(0, ch++));
                t = setTimeout(typeIt, 1000 / tot);
            }
        }

        function glitchIt() {
            setTimeout(
                function () {
                    $typer.removeData('text');
                    $typer.removeClass('glitchOn');
                }, 500);
        }
    });
}