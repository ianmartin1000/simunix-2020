$(document).ready(function(){
    /* SET UP CAROUSEL */
    $('.slider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
    });

    $('.slider-mobile').slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
    });


    $('.t2aslider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4
    });

    $('.t2aslider-mobile').slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    $('.ukpbslider-mobile').slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
    });

    $('.orbisslider-mobile').slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
    });

    /* NAVIGATION FADE BG */
    if($('body.home').length != 0) {
        var navBGVisible = false;
        $('body.home #primaryNav').on('mouseenter', function(e){
            if(navBGVisible == false) {
                $(this).css({"border-width":"0"}).animate({ backgroundColor: "rgba(0,0,0,1)"});
                navBGVisible = true;
            }
        }).on('mouseleave', function(e){
            if(navBGVisible == true && $(window).scrollTop() <= 0) {
                $(this).css({"border-width":"1px"}).animate({ backgroundColor: "rgba(0,0,0,0)"});
                navBGVisible = false;
            }
        });

        $(window).scroll(function(){
            if($(window).scrollTop() > 0) {
                if(navBGVisible == false) {
                    $('body.home #primaryNav').css({"border-width":"0"}).animate({ backgroundColor: "rgba(0,0,0,1)"});
                    navBGVisible = true;
                }
            } else {
                if(navBGVisible == true) {
                    $('body.home #primaryNav').css({"border-width":"1px"}).animate({ backgroundColor: "rgba(0,0,0,0)"});
                    navBGVisible = false;
                }
            }
        });
    }

    /* STICKY PAGENAV */
    if($('.pagenavcontainer').length != 0) {
        var pageNavStuck = false;
        $(window).scroll(function(){
            var offsetAmount = 78;
            if($(window).width() < 991) {
                offsetAmount = 128;
            }
            var pagenavTop = $('.pagenavcontainer').offset().top;
            var jumboBottom = ($('.jumbotron-fluid').offset().top + $('.jumbotron-fluid').outerHeight()) - offsetAmount;
            if(pagenavTop - $(window).scrollTop() <= offsetAmount) {
                if(pageNavStuck == false) {
                    $('.pagenavcontainer').css ({
                        "position":"fixed",
                        "top": offsetAmount + "px"
                    })
                    $('.pagenavcontainer').next().css({"padding-top":"+=48px"});
                    pageNavStuck = true;
                }
            }

            if(jumboBottom - $(window).scrollTop() >= 0) {
                if(pageNavStuck == true) {
                    $('.pagenavcontainer').css ({
                        "position":"relative",
                        "top":"0"
                    })
                    $('.pagenavcontainer').next().css({"padding-top":"-=48px"});
                    pageNavStuck = false;
                }
            }
        });
    }

    /* PAGENAV FUNCTIONALITY */
    $('.pagenav a').on('click', function(e){
        e.preventDefault();
        var location = $(this).attr("href");
        var offsetAmount = 96;
        if($(window).width() < 991) {
            offsetAmount = 126;
        }

        var $activeLink = $(this);
        $('html, body').animate({scrollTop: $(location).offset().top - offsetAmount }, 'slow', function(){
            $('.pagenav a').removeClass('active');
            $activeLink.addClass('active');
        });
    });

    var locationsObj = {};

    $('.container-fluid').each(function(){
        if(typeof $(this).attr("id") !== "undefined") {
            var element = "#" + $(this).attr("id");
            locationsObj[element] = $(element).offset().top;
        }
    });

    $(window).scroll(function() {
        var offsetAmount = 96;
        if($(window).width() < 991) {
            offsetAmount = 126;
        }
        var activeElement;

        $.each(locationsObj, function(element, location){
            if ($(window).scrollTop() + offsetAmount >= location - 10) {
                activeElement = element;
            }
        });

        $('.pagenav a').removeClass('active');
        $('.pagenav a[href="' + activeElement + '"]').addClass('active');
        if($('.pagenav a.active').length == 0) {
            $('.pagenav a').first().addClass('active');
        }

    });



    /* ENSURE ALL SLIDE HEADS EQUAL HEIGHT */
    var tallestSlide = 0;
    $('.slider .slide-head').each(function(){
       if(tallestSlide < $(this).height()) {
           tallestSlide =  $(this).height();
       }
    });
    $('.slider .slide-head').height(tallestSlide);

    var tallestSlide = 0;
    $('.t2aslider .slide-head').each(function(){
       if(tallestSlide < $(this).height()) {
           tallestSlide =  $(this).height();
       }
    });
    $('.t2aslider .slide-head').height(tallestSlide);


    /* HOMEPAGE HEADING TYPEWRITER */
    if($('body').hasClass('home')){
        var app = document.getElementById('searchTypewriter');

        var typewriter = new Typewriter(app, {
            loop: true,
            cursorClassName: "hideme"
        });

        typewriter.typeString('People')
            .pauseFor(6000)
            .deleteAll()
            .callFunction(function(){
                $('.jumbotron p.lead').hide();
                $('.jumbotron p.lead.businesses').fadeIn();
            })
            .typeString('Businesses')
            .pauseFor(6000)
            .deleteAll()
            .callFunction(function(){
                $('.jumbotron p.lead').hide();
                $('.jumbotron p.lead.addresses').fadeIn();
            })
            .typeString('Addresses')
            .pauseFor(6000)
            .deleteAll()
            .callFunction(function(){
                $('.jumbotron p.lead').hide();
                $('.jumbotron p.lead.people').fadeIn();
            })
            .start();
    }

    //Mobile navigation enhancements
    $('.navbar-toggler').on('click', function(){
        $('body').toggleClass('noscroll');
    });

    //sliders
    $('.gallery-slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        responsive: [
            {
                breakpoint: 1350,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('#isoDropdown').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        $('.'+$(this).attr('id')).slideToggle();
    });

    $('.accred').on('click', function(){
        var clickable = true;
        if($(this).hasClass('accred1')) {
            url = "https://certcheck.ukas.com/certification/a9f8019e-d4b8-5ed4-aeaa-4be41e3ec301";
        }

        if($(this).hasClass('accred3')) {
            url = baseUrl + '/media/certs/cep.pdf';
        }

        if($(this).hasClass('accred4')) {
            clickable = false;
        }

        if($(this).hasClass('accred5')) {
            url = baseUrl + '/media/certs/pci.pdf';
        }

        if(clickable) {
            window.open(url, '_blank').focus();
        }
    });
});