$(document).ready(function() {

    var location = false;
    if(window.location.href.search("t2a") != -1) {
        location = $('#contactUs');
    }
    else if(window.location.href.search("orbis") != -1) {
        location = $('#pricing');
    }

    if(location != false) {
        if(window.location.href.search("[?&]m=") != -1) {
            var offsetAmount = 96;
            if ($(window).width() < 991) {
                offsetAmount = 126;
            }

            $('html, body').animate({scrollTop: $(location).offset().top - offsetAmount}, 'slow');
        }
    }
});