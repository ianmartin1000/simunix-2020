$(document).ready(function(){
    
    //Homepage gallery
    $('.home-gallery .next').on('click', function(e){
        e.preventDefault();

        if($(this).hasClass('corp')) {
            //Change heading
            $('.gallery-heading').html('Organisations');

            //Change subheading
            $('.gallery-subheading-consumers').hide();
            $('.gallery-subheading-corporate').fadeIn("fast");

            //Change next button text
            $('.nexttextlabel').html('Developers');

            //Change images
            $('.gallery-right-consumers').hide();
            $('.gallery-left1-developers').hide();
            $('.gallery-left2-corporate').hide();
            $('.gallery-right-corporate').fadeIn("fast", function(){
                $('.gallery-left1-consumers').fadeIn("fast", function(){
                    $('.gallery-left2-developers').fadeIn("fast");
                });
            });

            //Change copy
            $('.grid-item1-consumers').hide();
            $('.grid-item2-consumers').hide();
            $('.grid-item3-consumers').hide();
            $('.grid-item4-consumers').hide();
            $('.grid-item1-corporate').fadeIn("fast", function(){
                $('.grid-item2-corporate').fadeIn("fast", function(){
                    $('.grid-item3-corporate').fadeIn("fast", function(){
                        $('.grid-item4-corporate').fadeIn("fast");
                    });
                });
            });

            $(this).removeClass('corp');
            $(this).addClass('dev');

        }else if($(this).hasClass('dev')) {
            //Change heading
            $('.gallery-heading').html('Developers');

            //Change subheading
            $('.gallery-subheading-corporate').hide();
            $('.gallery-subheading-developers').fadeIn("fast");

            //Change next button text
            $('.nexttextlabel').html('Consumers');

            //Change images
            $('.gallery-right-corporate').hide();
            $('.gallery-left1-consumers').hide();
            $('.gallery-left2-developers').hide();
            $('.gallery-right-developers').fadeIn("fast", function(){
                $('.gallery-left1-corporate').fadeIn("fast", function(){
                    $('.gallery-left2-consumers').fadeIn("fast");
                });
            });

            //Change copy
            $('.grid-item1-corporate').hide();
            $('.grid-item2-corporate').hide();
            $('.grid-item3-corporate').hide();
            $('.grid-item4-corporate').hide();
            $('.grid-item1-developers').fadeIn("fast", function(){
                $('.grid-item2-developers').fadeIn("fast", function(){
                    $('.grid-item3-developers').fadeIn("fast", function(){
                        $('.grid-item4-developers').fadeIn("fast");
                    });
                });
            });

            $(this).removeClass('dev');
            $(this).addClass('cons');
        }else if($(this).hasClass('cons')) {
            //Change heading
            $('.gallery-heading').html('Consumers');

            //Change subheading
            $('.gallery-subheading-developers').hide();
            $('.gallery-subheading-consumers').fadeIn("fast");

            //Change next button text
            $('.nexttextlabel').html('Organisations');

            //Change images
            $('.gallery-right-developers').hide();
            $('.gallery-left1-corporate').hide();
            $('.gallery-left2-consumers').hide();
            $('.gallery-right-consumers').fadeIn("fast", function(){
                $('.gallery-left1-developers').fadeIn("fast", function(){
                    $('.gallery-left2-corporate').fadeIn("fast");
                });
            });

            //Change copy
            $('.grid-item1-developers').hide();
            $('.grid-item2-developers').hide();
            $('.grid-item3-developers').hide();
            $('.grid-item4-developers').hide();
            $('.grid-item1-consumers').fadeIn("fast", function(){
                $('.grid-item2-consumers').fadeIn("fast", function(){
                    $('.grid-item3-consumers').fadeIn("fast", function(){
                        $('.grid-item4-consumers').fadeIn("fast");
                    });
                });
            });

            $(this).removeClass('cons');
            $(this).addClass('corp');
        }
    });

    //Orbis gallery
    $('.orbis-gallery .next').on('click', function(e){
        e.preventDefault();
        $('.findout').hide();
        if($(this).hasClass('prop')) {
            //Change heading
            $('.gallery-heading').html('Property &amp; Lettings');

            //Change next button text
            $('.nexttextlabel').html('Financial Services');

            //Change images
            $('.gallery-right-police').hide();
            $('.gallery-left1-financial').hide();
            $('.gallery-left2-property').hide();
            $('.gallery-right-property').fadeIn("fast", function(){
                $('.gallery-left1-people').fadeIn("fast", function(){
                    $('.gallery-left2-financial').fadeIn("fast");
                });
            });

            $('.findout.prop').fadeIn().css("display","inline-block");

            //Change copy
            $('.gallery-text-police').hide();
            $('.gallery-text-property').fadeIn("fast");

            $(this).removeClass('prop');
            $(this).addClass('fin');

        }else if($(this).hasClass('fin')) {
            //Change heading
            $('.gallery-heading').html('Financial Services');

            //Change next button text
            $('.nexttextlabel').html('People Tracing &amp; Debt Collection');

            //Change images
            $('.gallery-right-property').hide();
            $('.gallery-left1-people').hide();
            $('.gallery-left2-financial').hide();
            $('.gallery-right-financial').fadeIn("fast", function(){
                $('.gallery-left1-logistics').fadeIn("fast", function(){
                    $('.gallery-left2-people').fadeIn("fast");
                });
            });

            //Change copy
            $('.gallery-text-property').hide();
            $('.gallery-text-financial').fadeIn("fast");

            $(this).removeClass('fin');
            $(this).addClass('people');
        }else if($(this).hasClass('people')) {
            //Change heading
            $('.gallery-heading').html('People Tracing & Debt Collection');

            //Change next button text
            $('.nexttextlabel').html('Logistics');

            //Change images
            $('.gallery-right-financial').hide();
            $('.gallery-left1-logistics').hide();
            $('.gallery-left2-people').hide();
            $('.gallery-right-people').fadeIn("fast", function(){
                $('.gallery-left1-legal').fadeIn("fast", function(){
                    $('.gallery-left2-logistics').fadeIn("fast");
                });
            });

            //Change copy
            $('.gallery-text-financial').hide();
            $('.gallery-text-people').fadeIn("fast");

            $(this).removeClass('people');
            $(this).addClass('logi');
        }else if($(this).hasClass('logi')) {
            //Change heading
            $('.gallery-heading').html('Logistics');

            //Change next button text
            $('.nexttextlabel').html('Legal services');

            //Change images
            $('.gallery-right-people').hide();
            $('.gallery-left1-legal').hide();
            $('.gallery-left2-logistics').hide();
            $('.gallery-right-logistics').fadeIn("fast", function(){
                $('.gallery-left1-police').fadeIn("fast", function(){
                    $('.gallery-left2-legal').fadeIn("fast");
                });
            });

            $('.findout.logistics').fadeIn().css("display","inline-block");

            //Change copy
            $('.gallery-text-people').hide();
            $('.gallery-text-logistics').fadeIn("fast");

            $(this).removeClass('logi');
            $(this).addClass('leg');
        }else if($(this).hasClass('leg')) {
            $('.findout.legal').fadeIn().css("display","inline-block");
            //Change heading
            $('.gallery-heading').html('Legal Services');

            //Change next button text
            $('.nexttextlabel').html('Police &amp; Security');

            //Change images
            $('.gallery-right-logistics').hide();
            $('.gallery-left1-police').hide();
            $('.gallery-left2-legal').hide();
            $('.gallery-right-legal').fadeIn("fast", function(){
                $('.gallery-left1-property').fadeIn("fast", function(){
                    $('.gallery-left2-police').fadeIn("fast");
                });
            });

            //Change copy
            $('.gallery-text-logistics').hide();
            $('.gallery-text-legal').fadeIn("fast");

            $(this).removeClass('leg');
            $(this).addClass('pol');
        }else if($(this).hasClass('pol')) {
            //Change heading
            $('.gallery-heading').html('Police &amp; Security');

            //Change next button text
            $('.nexttextlabel').html('Property &amp; Lettings');

            //Add landing page link
            $('.findout.police').fadeIn().css("display","inline-block");

            //Change images
            $('.gallery-right-legal').hide();
            $('.gallery-left1-property').hide();
            $('.gallery-left2-police').hide();
            $('.gallery-right-police').fadeIn("fast", function(){
                $('.gallery-left1-financial').fadeIn("fast", function(){
                    $('.gallery-left2-property').fadeIn("fast");
                });
            });

            //Change copy
            $('.gallery-text-legal').hide();
            $('.gallery-text-police').fadeIn("fast");

            $(this).removeClass('pol');
            $(this).addClass('prop');
        }
    });

    //Legal gallery
    $('.legal-gallery .next').on('click', function(e){
        e.preventDefault();

        if($(this).hasClass('prop')) {
            //Change heading
            $('.gallery-heading').html('Property Searches &amp; Home Owner verification');

            //Change next button text
            $('.nexttextlabel').html('Searching UK company information');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-property').fadeIn("fast", function(){
                $('.gallery-left1-people').fadeIn("fast", function(){
                    $('.gallery-left2-financial').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-property').fadeIn("fast");

            $(this).removeClass('prop');
            $(this).addClass('fin');

        }else if($(this).hasClass('fin')) {
            //Change heading
            $('.gallery-heading').html('Searching UK company information');

            //Change next button text
            $('.nexttextlabel').html('Person searching &amp; Identity verification');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-financial').fadeIn("fast", function(){
                $('.gallery-left1-property').fadeIn("fast", function(){
                    $('.gallery-left2-people').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-financial').fadeIn("fast");

            $(this).removeClass('fin');
            $(this).addClass('people');
        }else if($(this).hasClass('people')) {
            //Change heading
            $('.gallery-heading').html('Person searching &amp; Identity verification');

            //Change next button text
            $('.nexttextlabel').html('Property Search &amp; Home Owner Verify');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-people').fadeIn("fast", function(){
                $('.gallery-left1-financial').fadeIn("fast", function(){
                    $('.gallery-left2-property').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-people').fadeIn("fast");

            $(this).removeClass('people');
            $(this).addClass('prop');
        }
    });

    //Estate Agents gallery
    $('.estates-gallery .next').on('click', function(e){
        e.preventDefault();

        if($(this).hasClass('verify')) {
            //Change heading
            $('.gallery-heading').html('Verify new customers');

            //Change next button text
            $('.nexttextlabel').html('Local Marketing ZoneSearch');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-verify').fadeIn("fast", function(){
                $('.gallery-left1-database').fadeIn("fast", function(){
                    $('.gallery-left2-zonesearch').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-verify').fadeIn("fast");

            $(this).removeClass('verify');
            $(this).addClass('zonesearch');

        }else if($(this).hasClass('zonesearch')) {
            //Change heading
            $('.gallery-heading').html('Local Marketing ZoneSearch');

            //Change next button text
            $('.nexttextlabel').html('Enhance your marketing database');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-zonesearch').fadeIn("fast", function(){
                $('.gallery-left1-landlords').fadeIn("fast", function(){
                    $('.gallery-left2-database').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-zonesearch').fadeIn("fast");

            $(this).removeClass('zonesearch');
            $(this).addClass('database');
        }else if($(this).hasClass('database')) {
            //Change heading
            $('.gallery-heading').html('Enhance your marketing database');

            //Change next button text
            $('.nexttextlabel').html('Find Landlord details');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-database').fadeIn("fast", function(){
                $('.gallery-left1-verify').fadeIn("fast", function(){
                    $('.gallery-left2-landlords').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-database').fadeIn("fast");

            $(this).removeClass('database');
            $(this).addClass('landlords');
        }else if($(this).hasClass('landlords')) {
            //Change heading
            $('.gallery-heading').html('Find Landlord details');

            //Change next button text
            $('.nexttextlabel').html('Verify new customers');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-landlords').fadeIn("fast", function(){
                $('.gallery-left1-zonesearch').fadeIn("fast", function(){
                    $('.gallery-left2-verify').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-landlords').fadeIn("fast");

            $(this).removeClass('landlords');
            $(this).addClass('verify');
        }
    });

    //Police gallery
    $('.police-gallery .next').on('click', function(e){
        e.preventDefault();

        if($(this).hasClass('idv')) {
            //Change heading
            $('.gallery-heading').html('Identity Verification');

            //Change next button text
            $('.nexttextlabel').html('OSINT Investigations');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-idv').fadeIn("fast", function(){
                $('.gallery-left1-brsmn').fadeIn("fast", function(){
                    $('.gallery-left2-osint').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-idv').fadeIn("fast");

            $(this).removeClass('idv');
            $(this).addClass('osint');

        }else if($(this).hasClass('osint')) {
            //Change heading
            $('.gallery-heading').html('OSINT Investigations');

            //Change next button text
            $('.nexttextlabel').html('Reverse Search Mobile Numbers');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-osint').fadeIn("fast", function(){
                $('.gallery-left1-ig').fadeIn("fast", function(){
                    $('.gallery-left2-brsmn').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-osint').fadeIn("fast");

            $(this).removeClass('osint');
            $(this).addClass('brsmn');
        }else if($(this).hasClass('brsmn')) {
            //Change heading
            $('.gallery-heading').html('Bulk Reverse Search Mobile Numbers');

            //Change next button text
            $('.nexttextlabel').html('Intelligence Gathering');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-brsmn').fadeIn("fast", function(){
                $('.gallery-left1-lr').fadeIn("fast", function(){
                    $('.gallery-left2-ig').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-brsmn').fadeIn("fast");

            $(this).removeClass('brsmn');
            $(this).addClass('ig');
        }else if($(this).hasClass('ig')) {
            //Change heading
            $('.gallery-heading').html('Intelligence Gathering in high crime areas');

            //Change next button text
            $('.nexttextlabel').html('Land Registry');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-ig').fadeIn("fast", function(){
                $('.gallery-left1-pt').fadeIn("fast", function(){
                    $('.gallery-left2-lr').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-ig').fadeIn("fast");

            $(this).removeClass('ig');
            $(this).addClass('lr');
        }else if($(this).hasClass('lr')) {
            //Change heading
            $('.gallery-heading').html('Augmented Land Registry information');

            //Change next button text
            $('.nexttextlabel').html('Person Tracing');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-lr').fadeIn("fast", function(){
                $('.gallery-left1-idv').fadeIn("fast", function(){
                    $('.gallery-left2-pt').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-lr').fadeIn("fast");

            $(this).removeClass('lr');
            $(this).addClass('pt');
        }else if($(this).hasClass('pt')) {
            //Change heading
            $('.gallery-heading').html('Person Tracing');

            //Change next button text
            $('.nexttextlabel').html('Identity Verification');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-pt').fadeIn("fast", function(){
                $('.gallery-left1-osint').fadeIn("fast", function(){
                    $('.gallery-left2-idv').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-pt').fadeIn("fast");

            $(this).removeClass('pt');
            $(this).addClass('idv');
        }
    });

    //Logistics gallery
    $('.logistics-gallery .next').on('click', function(e){
        e.preventDefault();

        if($(this).hasClass('vmpd')) {
            //Change heading
            $('.gallery-heading').html('Verify mobile phone data of customers');

            //Change next button text
            $('.nexttextlabel').html('Ordnance Survey maps');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-vmpd').fadeIn("fast", function(){
                $('.gallery-left1-vadrd').fadeIn("fast", function(){
                    $('.gallery-left2-os').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-vmpd').fadeIn("fast");

            $(this).removeClass('vmpd');
            $(this).addClass('os');

        }else if($(this).hasClass('os')) {
            //Change heading
            $('.gallery-heading').html('Make deliveries using Ordnance Survey maps');

            //Change next button text
            $('.nexttextlabel').html('Verify age');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-os').fadeIn("fast", function(){
                $('.gallery-left1-vnd').fadeIn("fast", function(){
                    $('.gallery-left2-vadrd').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-os').fadeIn("fast");

            $(this).removeClass('os');
            $(this).addClass('vadrd');
        }else if($(this).hasClass('vadrd')) {
            //Change heading
            $('.gallery-heading').html('Verify age for delivery of restricted goods');

            //Change next button text
            $('.nexttextlabel').html('Validate neighbour details');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-vadrd').fadeIn("fast", function(){
                $('.gallery-left1-aml').fadeIn("fast", function(){
                    $('.gallery-left2-vnd').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-vadrd').fadeIn("fast");

            $(this).removeClass('vadrd');
            $(this).addClass('vnd');
        }else if($(this).hasClass('vnd')) {
            //Change heading
            $('.gallery-heading').html('Validate neighbour details');

            //Change next button text
            $('.nexttextlabel').html('AML checks');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-vnd').fadeIn("fast", function(){
                $('.gallery-left1-kyc').fadeIn("fast", function(){
                    $('.gallery-left2-aml').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-vnd').fadeIn("fast");

            $(this).removeClass('vnd');
            $(this).addClass('aml');
        }else if($(this).hasClass('aml')) {
            //Change heading
            $('.gallery-heading').html('Conduct AML checks on businesses');

            //Change next button text
            $('.nexttextlabel').html('Know your customer');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-aml').fadeIn("fast", function(){
                $('.gallery-left1-vna').fadeIn("fast", function(){
                    $('.gallery-left2-kyc').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-aml').fadeIn("fast");

            $(this).removeClass('aml');
            $(this).addClass('kyc');
        }else if($(this).hasClass('kyc')) {
            //Change heading
            $('.gallery-heading').html('KYC check for new customers');

            //Change next button text
            $('.nexttextlabel').html('Verifying name &amp; address');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-kyc').fadeIn("fast", function(){
                $('.gallery-left1-vmpd').fadeIn("fast", function(){
                    $('.gallery-left2-vna').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-aml').fadeIn("fast");

            $(this).removeClass('kyc');
            $(this).addClass('vna');
        }else if($(this).hasClass('vna')) {
            //Change heading
            $('.gallery-heading').html('Verifying name &amp; address data');

            //Change next button text
            $('.nexttextlabel').html('Verify mobile phone data');

            //Change images
            $('[class*="gallery-right-"]').hide();
            $('[class*="gallery-left1-"]').hide();
            $('[class*="gallery-left2-"]').hide();
            $('.gallery-right-vna').fadeIn("fast", function(){
                $('.gallery-left1-os').fadeIn("fast", function(){
                    $('.gallery-left2-vmpd').fadeIn("fast");
                });
            });

            //Change copy
            $('[class*="gallery-text-"]').hide();
            $('.gallery-text-vna').fadeIn("fast");

            $(this).removeClass('vna');
            $(this).addClass('vmpd');
        }
    });

    //Logistics gallery links
    $('.gallery-links a').on('click', function(e){
        e.preventDefault();
        $('a[href="#useCases"]').click();
        $('.gallery-link a').removeClass();
        $('.gallery-link a').addClass('next');
        $('.gallery-link a').addClass($(this).data('gallery-item'));
        $('.gallery-link .next').click();
    });

    //Body links fire next events
    $('.prop-inner').on('click', function(e){
        e.preventDefault();
        $('.next').attr('class', 'next prop');
        $('.next').trigger('click');
    });
    $('.fin-inner').on('click', function(e){
        e.preventDefault();
        $('.next').attr('class', 'next fin');
        $('.next').trigger('click');
    });
    $('.people-inner').on('click', function(e){
        e.preventDefault();
        $('.next').attr('class', 'next people');
        $('.next').trigger('click');
    });


});